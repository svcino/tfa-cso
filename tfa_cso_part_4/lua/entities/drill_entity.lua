ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "Needler Nail"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.HitDamage = 1500
ENT.PenetrationPower = 70
ENT.PenetrationPowerScale = 0.33
ENT.PenetrationDamageScale = 0.75
ENT.MaxPenetration = 5
ENT.AliveTime = 5
ENT.IgnoreDamage = true

ENT.Impacted = false
ENT.ImpactVec = Vector(0, 0, 0)
ENT.BaseAngle = Angle(0, 0, 0)
ENT.ImpactCount = 0
ENT.KillTime = -1

if SERVER then

	AddCSLuaFile()

	function ENT:GetPenetratable(pos, dir)
		return (util.IsInWorld(pos + dir * self.PenetrationPower))
	end

	function ENT:GetNextPenetrationPos(pos, dir)
		local mins, maxs = self:GetCollisionBounds()
		local dst = mins:Distance(maxs) * 5
		local tr = {
			start = pos + dir * self.PenetrationPower,
			endpos = pos,
			mask = MASK_SHOT,
			filter = self,
		}
		local ret = util.TraceLine(tr)
		return ret.HitPos + (dir * dst)
	end

	function ENT:Initialize()
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(false)
		self:SetCollisionGroup(0)
		local mn, mx = self:GetModelBounds()
		self:SetCollisionBounds(mn, mx)
		self:SetMoveType(MOVETYPE_FLY)
		self:PhysicsInit(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
		if(IsValid(phys)) then
			phys:Wake()
			phys:EnableGravity(false)
			phys:SetVelocity(self:GetAngles():Forward() * 4000)
		end
		util.SpriteTrail(self, 0, Color(255, 255, 255), false, 1, 0.0, 0.15, 0, "effects/beam_generic01")
		self.BaseAngle = self:GetAngles()
		self.KillTime = CurTime() + self.AliveTime
	end

	function ENT:Think()
		if(self.KillTime < CurTime()) then self:Remove() end
		self:SetAngles(self.BaseAngle)
		local phys = self:GetPhysicsObject()
		if(!self.Impacted) then
			phys:SetVelocity(self:GetAngles():Forward() * 4000)
		else
			self:SetPos(self.ImpactVec)
		end
		self:NextThink(CurTime())
		return true
	end

	function ENT:StopMotion()
		local phys = self:GetPhysicsObject()
		phys:EnableMotion(false)
		phys:EnableCollisions(false)
		self.Impacted = true
	end

	function ENT:PhysicsCollide(data, phys)
		local HitPos = data.HitPos
		local HitEntity = data.HitEntity
		local HitDirectionVec = self.BaseAngle:Forward()
		self.ImpactVec = HitPos
		if(IsValid(HitEntity)) then
			HitEntity:TakeDamageInfo(CSO:DMGInfo(self.Owner, CSO:GetInflictor(self.Owner), self.HitDamage, HitDirectionVec * 500, DMG_SLASH, CSO:GetEntityCenter(HitEntity)))
		end
		if(self.ImpactCount >= self.MaxPenetration) then
			self:StopMotion()
		else
			if(self:GetPenetratable(HitPos, HitDirectionVec)) then
				self:SetPos(self:GetNextPenetrationPos(HitPos, HitDirectionVec))
				self.PenetrationPower = self.PenetrationPower * self.PenetrationPowerScale
				if(self.ImpactCount > 0) then
					self.HitDamage = self.HitDamage * self.PenetrationDamageScale
				end
				timer.Simple(0.0, function() self:SetAngles(self.BaseAngle) end)
			else
				self:StopMotion()
			end
		end
		self.ImpactCount = self.ImpactCount + 1
	end
end