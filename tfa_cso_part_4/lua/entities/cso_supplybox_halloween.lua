AddCSLuaFile()

ENT.Base = "cso_supplybox_base"

ENT.PrintName = "Halloween Supply Box"
ENT.Category = "TFA CS:O Supply Boxes"
ENT.Spawnable = true
ENT.AdminSpawnable = true
ENT.MyModel = "models/tfa_cso/entities/supplybox3.mdl"
ENT.ImpactSound = "Plastic_Box.ImpactSoft"
ENT.ShouldDrawShadow = true

ENT.GiveAmmo = true -- give ammo?
ENT.AmmoMultiplier = 5 -- ammo to give if GiveAmmo is true

ENT.GiveArmor = true -- give armor?
ENT.ArmorGiven = 75 -- Armor given if GiveArmor is true
ENT.ArmorLimit = true -- limit max armor this ent will give? if set, this will override player:GetMaxArmor()
ENT.ArmorLimitMax = 75 -- if ArmorLimit is true, limit armor given to this value

ENT.GiveHealth = true -- give health?
ENT.HealthGiven = 100 -- health given if GiveHealth is true

ENT.GiveOnTouch = false -- give stuff if touched?
ENT.SpawnedSound = "entities/tfa_cso/supply_box/spawn.wav" -- sound to play when spawned
ENT.UsedSound = "entities/tfa_cso/supply_box/pickup.wav" -- sound to play when used

ENT.GiveRandomWeaponAmmo = true -- give ammo for the randomly acquired weapon?
ENT.GiveRandomWeaponAmmoMultiplier = 2 -- multiplier for ammo given

ENT.GiveSupplyBoxRandomWeapon = true -- give a random weapon on use?
ENT.UseRandomWeaponTable = true -- use ENT.RandomWeaponCategories instead of every category?
ENT.RandomWeaponTable = { -- THIS IS A TABLE. classnames to use for giving random weapons. Only works if ENT.GiveRandomWeapon and ENT.UseRandomWeaponTable are true.
 "tfa_cso_ak_long",
 "tfa_cso_ddeagle",
 "tfa_cso_dmp7a1",
 "tfa_cso_dualnata",
 "tfa_cso_poisongun",
 "tfa_cso_automagv",
 "tfa_cso_dualkriss",
 "tfa_cso_m95ghost",
 "tfa_cso_mk48",
 "tfa_cso_kriss",
 "tfa_cso_railcannon",
 "tfa_cso_psg1",
 "tfa_cso_tar21"
}