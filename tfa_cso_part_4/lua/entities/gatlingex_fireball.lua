ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "Fireball"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false


ENT.MyModel = "models/hunter/misc/sphere1x1.mdl"
ENT.MyModelScale = 1
ENT.Damage = 2000
ENT.Radius = 256
if SERVER then

	AddCSLuaFile()

	function ENT:Initialize()

		local model = self.MyModel and self.MyModel or "models/hunter/misc/sphere1x1.mdl"
		
		self.Class = self:GetClass()
		
		self:SetModel(model)
		
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(true)
		self:SetCollisionGroup(1)
	    self:SetMaterial( "phoenix_storms/gear" )		
		self:SetHealth(1)
		self:SetModelScale(self.MyModelScale,0)	
		local phys = self:GetPhysicsObject()
		if IsValid(phys) then
			phys:Wake()
			phys:SetMass(1)
			phys:EnableDrag(false)
			phys:EnableGravity(false)
			phys:SetBuoyancyRatio(0)
		end		
		local phys = self:GetPhysicsObject()
		if (phys:IsValid()) then
			phys:Wake()
		end
		self.KillTime = CurTime() + 5
	end
	
	function ENT:Think()
		if(!IsValid(self.Owner)) then self:Remove() end
		if(self.KillTime < CurTime()) then
			local owent = self.Owner and self.Owner or self
			util.BlastDamage(self,owent,self:GetPos(),self.Radius,self.Damage)
			local fx = EffectData()
			fx:SetOrigin(self:GetPos())
			util.Effect("exp_gatlingex",fx)
			self:Remove()
		end
		CSO:DoRadiusAttack_WeaponLess(self.Owner, CSO:DMGInfo(self.Owner, CSO:GetInflictor(self.Owner), 30, self:GetAngles():Forward() * -128, DMG_SHOCK, Vector(0, 0, 0)), 100, false, 0, 1, true, self:GetPos(), false, true, 2, 1.5, true)
		self:NextThink(CurTime() + 0.1)
		return true
	end
	
	function ENT:PhysicsCollide(data, physobj)
		local owent = self.Owner and self.Owner or self
		util.BlastDamage(self,owent,self:GetPos(),self.Radius,self.Damage)
		local fx = EffectData()
		fx:SetOrigin(self:GetPos())
		fx:SetNormal(data.HitNormal)
		util.Effect("exp_gatlingex",fx)
		self.Entity:EmitSound("Volcanoex.Exp", self.Pos, 100, 100 )
		self:Remove()
	end
end

if CLIENT then
	
	function ENT:Draw()
		self:DrawModel()
	end

end

function ENT:Draw()
render.SetMaterial(Material("sprites/ef_gatlingex_fireball"))
render.DrawSprite(self.Entity:GetPos() + ((Vector(0,0,0))),256,256,Color(255, 255, 255))
end