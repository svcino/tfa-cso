ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "Needler Nail"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.Damage = 15
ENT.KillTime = -1

if SERVER then

	AddCSLuaFile()

	function ENT:Initialize()
		self:SetModel("models/weapons/tfa_cso/bouncer_bullet.mdl") -- Temporary model
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(false)
		self:SetModelScale(0, 0)
		self:SetCollisionGroup(0)
		local mn, mx = self:GetModelBounds()
		self:SetCollisionBounds(mn, mx)
		self:SetMoveType(MOVETYPE_FLY)
		self:PhysicsInit(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
		if(IsValid(phys)) then
			phys:Wake()
			phys:EnableGravity(false)
			phys:SetVelocity(self:GetAngles():Forward() * 5000)
		end
		util.SpriteTrail(self, 0, Color(255, 255, 255), false, 1, 0.0, 0.1, 0, "effects/beam_generic01")
		self.KillTime = CurTime() + 3
		self.HitDamage = 30
	end

	ENT.CollidedCounter = 0
	ENT.MaximumCollide = 5

	function ENT:Think()
		local phys = self:GetPhysicsObject()
		phys:SetVelocity(self:GetAngles():Forward() * 5000)
		self:NextThink(CurTime())
		return true
	end

	function ENT:Think()
		if(self.KillTime < CurTime()) then self:Remove() end
		self:NextThink(CurTime())
		if(self:GetVelocity():Length2D() < 3500) then
			self:GetPhysicsObject():SetVelocity(self:GetVelocity() + self:GetVelocity() * 0.3)
		end
		return true
	end

end