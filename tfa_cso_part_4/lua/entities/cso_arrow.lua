ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "Needler Nail"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false

ENT.HitDamage = 25

ENT.Impacted = false
ENT.ImpactVec = Vector(0, 0, 0)
ENT.ImpactOffset = Vector(0, 0, 0)
ENT.ImpactEntity = nil
ENT.BaseAngle = Angle(0, 0, 0)
ENT.ForwardAngle = Angle(0, 0, 0)
ENT.ImpactCount = 0
ENT.KillTime = -1

ENT.ForwardAngleOffset = Angle(0, 90, 0)
ENT.IgnoreDamage = true

if SERVER then

	AddCSLuaFile()

	function ENT:GetDamage()
		if(IsValid(self.Owner)) then
			if(self.Owner:IsPlayer()) then
				local w = self.Owner:GetActiveWeapon()
				if(IsValid(w)) then
					if(w.Primary.Damage != nil) then
						return w.Primary.Damage
					else
						return 25
					end
					else
						return 25
				end
			else
				return 25
			end
		else
			return 25
		end
	end

	function ENT:Initialize()
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(false)
		self:SetCollisionGroup(0)
		local mn, mx = self:GetModelBounds()
		self:SetCollisionBounds(mn, mx)
		self:SetMoveType(MOVETYPE_FLY)
		self:PhysicsInit(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
		if(IsValid(phys)) then
			phys:Wake()
			phys:EnableGravity(false)
			phys:SetVelocity(self:GetAngles():Forward() * 4000)
		end
		util.SpriteTrail(self, 0, Color(255, 255, 255), false, 1, 0.0, 0.15, 0, "effects/beam_generic01")
		self.HitDamage = self:GetDamage()
		self.BaseAngle = self:GetAngles() + self.ForwardAngleOffset + Angle(0, 0, self.Owner:EyeAngles().x)
		self.BaseAngle.x = 0
		self.ForwardAngle = self:GetAngles()
		self.KillTime = CurTime() + 3
		self:SetAngles(self.BaseAngle)
	end

	function ENT:Think()
		if(self.KillTime < CurTime()) then self:Remove() end
		self:SetAngles(self.BaseAngle)
		local phys = self:GetPhysicsObject()
		if(!self.Impacted) then
			phys:SetVelocity(self.ForwardAngle:Forward() * 4000)
			self.KillTime = CurTime() + 1
		else
			if(IsValid(self.ImpactEntity)) then
				self:SetPos(self.ImpactEntity:GetPos() + self.ImpactOffset)
			else
				self:SetPos(self.ImpactVec)
			end
		end
		self:NextThink(CurTime())
		return true
	end

	function ENT:StopMotion()
		local phys = self:GetPhysicsObject()
		self:SetCollisionGroup(1)
		phys:EnableMotion(false)
		phys:EnableCollisions(false)
	end

	function ENT:PhysicsCollide(data, phys)
		local HitPos = data.HitPos
		local HitEntity = data.HitEntity
		if(IsValid(HitEntity)) then
			self.ImpactEntity = HitEntity
			self.ImpactOffset = HitPos - HitEntity:GetPos()
			HitEntity:TakeDamageInfo(CSO:DMGInfo(self.Owner, CSO:GetInflictor(self.Owner), self.HitDamage, Vector(0, 0, 0), DMG_SLASH, CSO:GetEntityCenter(HitEntity)))
		end
		self.ImpactVec = HitPos
		self.Impacted = true
		self:StopMotion()
	end

end