SWEP.Base = "tfa_bash_base"
SWEP.Category = "TFA CS:O Equipment"
SWEP.Author = "Kamikaze, ★Bullet★"
SWEP.PrintName = "Ripper Expert"
SWEP.Type	= "Transcendent Grade Equipment"
SWEP.ViewModel = "models/weapons/tfa_cso/c_chainsaw_v6.mdl"
SWEP.ViewModelFOV = 80
SWEP.ViewModelFlip = true
SWEP.VMPos = Vector(0, 0, 0)
SWEP.UseHands = true
SWEP.ProceduralHolsterTime = 0

--SWEP.InspectPos = Vector(17.184, -4.891, -11.902) - SWEP.VMPos
--SWEP.InspectAng = Vector(70, 46.431, 70)
SWEP.WorldModel = "models/weapons/tfa_cso/w_chainsaw_v6.mdl"
SWEP.Offset = {
	Pos = {
		Up = 2,
		Right = 1.5,
		Forward = 15
	},
	Ang = {
		Up = -90,
		Right = -0,
		Forward = 170
	},
	Scale = 1
}
SWEP.HoldType = "shotgun"
SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.AllowSprintAttack = true
SWEP.IsMelee = true
SWEP.DisableChambering = true
SWEP.MuzzleFlashEnabled = false
SWEP.MoveSpeed = 1.05

SWEP.Primary.Automatic = true
SWEP.Primary.RPM = 1000
SWEP.Primary.Damage = 80
SWEP.Primary.NumShots = 1
SWEP.Primary.Ammo = "gasoline"
SWEP.SelectiveFire = false
SWEP.Primary.Reach = 100
SWEP.Primary.ClipSize = 300
SWEP.Primary.DefaultClip = 900

SWEP.data = {}
SWEP.data.ironsights = 0

SWEP.SawAnimation = {
	["in"] = {
		["type"] = TFA.Enum.ANIMATION_SEQ, --Sequence or act
		["value"] = "attack1_start", --Number for act, String/Number for sequence
		["transition"] = true
	}, --Inward transition
	["loop"] = {
		["type"] = TFA.Enum.ANIMATION_SEQ, --Sequence or act
		["value"] = "attack1_loop", --Number for act, String/Number for sequence
		["is_idle"] = true
	},--looping animation
	["out"] = {
		["type"] = TFA.Enum.ANIMATION_SEQ, --Sequence or act
		["value"] = "attack1_end", --Number for act, String/Number for sequence
		["transition"] = true
	} --Outward transition
}

SWEP.Saw_Sound_Idle = ""
SWEP.Saw_Sound_Saw = "Chainsaw.Attack_Loop"
SWEP.Saw_Sound_In = "Chainsaw.Attack_Start"
SWEP.Saw_Sound_Out = "Chainsaw.Attack_End"

SWEP.Saw_Sound_Idle_Next = -1
SWEP.Saw_Sound_Saw_Next = -1

SWEP.Saw_Sound_BlendTime = 0.05

SWEP.Saw_Drain_Idle = 0
SWEP.Saw_Drain_Sawing = 120 / 10--Ammo per second

SWEP.Secondary.Automatic = true
SWEP.Secondary.CanBash = true
SWEP.Secondary.BashDamage = 675
SWEP.Secondary.BashDelay = 0.1
SWEP.Secondary.BashLength = 90
SWEP.Secondary.BashDamageType = bit.bor(DMG_SLASH,DMG_ALWAYSGIB)

SWEP.Secondary.BashSound = Sound("")
SWEP.Secondary.BashHitSound = Sound("Chainsaw.Slash"..math.random(1,2))
SWEP.Secondary.BashHitSound_Flesh = Sound("Chainsaw.HitFleshSlash2")

-- nZombies Stuff
SWEP.NZWonderWeapon		= false	-- Is this a Wonder-Weapon? If true, only one player can have it at a time. Cheats aren't stopped, though.
--SWEP.NZRePaPText		= "your text here"	-- When RePaPing, what should be shown? Example: Press E to your text here for 2000 points.
SWEP.NZPaPName				= "Rip 'n Tear"
--SWEP.NZPaPReplacement 	= "tfa_cso_dualinfinityfinal"	-- If Pack-a-Punched, replace this gun with the entity class shown here.
SWEP.NZPreventBox		= true	-- If true, this gun won't be placed in random boxes GENERATED. Users can still place it in manually.
SWEP.NZTotalBlackList	= true	-- if true, this gun can't be placed in the box, even manually, and can't be bought off a wall, even if placed manually. Only code can give this gun.

SWEP.Base = "tfa_cso_chainsaw"

SWEP.BashKnockback = 1800

if CLIENT then
	SWEP.WepSelectIconCSO = Material("vgui/killicons/tfa_cso_chainsaw_v6")
	SWEP.DrawWeaponSelection = TFA_CSO_DrawWeaponSelection
end