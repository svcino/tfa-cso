ENT.Type 			= "anim"
ENT.Base 			= "base_anim"
ENT.PrintName		= "Laserminigun Plasma"
ENT.Category		= "None"

ENT.Spawnable		= false
ENT.AdminSpawnable	= false


ENT.MyModel = "models/items/ar2_grenade.mdl"
ENT.MyModelScale = 0
ENT.Damage = 1500
ENT.Radius = 200
ENT.MatSize = 1
ENT.MatType = 1
ENT.Material = nil

if SERVER then

	AddCSLuaFile()

	function ENT:Initialize()

		local model = self.MyModel and self.MyModel or "models/items/ar2_grenade.mdl"
		
		self.Class = self:GetClass()
		
		self:SetModel(model)
		self:PhysicsInit(SOLID_VPHYSICS)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		self:DrawShadow(false)
		self:SetHealth(1)
		self:SetCollisionGroup(COLLISION_GROUP_NONE)
		self:SetModelScale(self.MyModelScale,0)
        local phys = self:GetPhysicsObject()
		if IsValid(phys) then
			phys:Wake()
			phys:SetMass(1)
			phys:EnableDrag(false)
			phys:EnableGravity(false)
			phys:SetBuoyancyRatio(0)
		end
				
		local phys = self:GetPhysicsObject()
		
		if (phys:IsValid()) then
			phys:Wake()
		end
	end

	function ENT:PhysicsCollide(data, physobj)	
		util.BlastDamage(self.Owner,self.Owner,self:GetPos(),self.Radius,self.Damage)
		local fx = EffectData()
		fx:SetOrigin(self:GetPos())
		--fx:SetNormal(data.HitNormal)
		util.Effect("exp_laserplasma"..self:GetNWInt("CSO_MatType", 1),fx)
		self:Remove()
	end
end

if CLIENT then
	function ENT:Draw()
		self:DrawModel()
	end
end

function ENT:Draw()
	render.SetMaterial(Material("sprites/laserminigun_charge_"..self:GetNWInt("CSO_MatType", 1)))
	render.DrawSprite(self.Entity:GetPos() + ((Vector(0,0,0))),self:GetNWInt("CSO_MatSize", 1),self:GetNWInt("CSO_MatSize", 1),Color(255, 255, 255))
end