--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--Galil maverick
TFA.AddFireSound( "GalilCraft.Fire", "weapons/tfa_cso/galilcraft/fire.wav", false, "^" )
TFA.AddWeaponSound( "GalilCraft.ClipIn", "weapons/tfa_cso/galilcraft/clipin.wav")
TFA.AddWeaponSound( "GalilCraft.ClipOut", "weapons/tfa_cso/galilcraft/clipout.wav")
TFA.AddWeaponSound( "GalilCraft.Boltpull", "weapons/tfa_cso/galilcraft/boltpull.wav")

--Hunter Killer X-12
TFA.AddFireSound( "X-12.Fire", "weapons/tfa_cso/x-12/fire.wav", false, "^" )
TFA.AddWeaponSound( "X-12.Draw", "weapons/tfa_cso/x-12/draw.wav" )
TFA.AddWeaponSound( "X-12.ClipIn1", "weapons/tfa_cso/x-12/clipin1.wav" )
TFA.AddWeaponSound( "X-12.ClipIn2", "weapons/tfa_cso/x-12/clipin2.wav" )
TFA.AddWeaponSound( "X-12.ClipOut1", "weapons/tfa_cso/x-12/clipout1.wav" )
TFA.AddWeaponSound( "X-12.ClipOut2", "weapons/tfa_cso/x-12/clipout2.wav" )
TFA.AddWeaponSound( "X-12.Scan_Activate", "weapons/tfa_cso/x-12/scan_activate.wav" )
TFA.AddWeaponSound( "X-12.Scan_Deactivate", "weapons/tfa_cso/x-12/scan_deactivate.wav" )
TFA.AddWeaponSound( "X-12.Scan_Zoom", "weapons/tfa_cso/x-12/zoom.wav" )
TFA.AddWeaponSound( "X-12.Beep", "weapons/tfa_cso/x-12/beep.wav" )


//StarChaser AR. Shooting AUG

TFA.AddFireSound( "StarAR.Fire2", "weapons/tfa_cso/starchaserar/fire2.wav", false, "^" )

local soundData = {
    name        = "StarAR.Idle" ,
    channel     = CHAN_WEAPON,
    volume      = 1,
    soundlevel  = 80,
    pitchstart  = 100,
    pitchend    = 100,
    sound       = "weapons/tfa_cso/starchaserar/idle.wav"
}

sound.Add(soundData)

--Electron-V
TFA.AddFireSound( "ElectronV.Fire", "weapons/tfa_cso/electronv/fire.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Fire2", "weapons/tfa_cso/electronv/fire2.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Fire3", "weapons/tfa_cso/electronv/fire3.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Exp2", "weapons/tfa_cso/electronv/exp2.wav", false, "^" )
TFA.AddFireSound( "ElectronV.Exp3", "weapons/tfa_cso/electronv/exp3.wav", false, "^" )
TFA.AddWeaponSound( "ElectronV.Draw", "weapons/tfa_cso/electronv/draw.wav" )
TFA.AddWeaponSound( "ElectronV.Idle1", "weapons/tfa_cso/electronv/idle1.wav" )
TFA.AddWeaponSound( "ElectronV.Idle2", "weapons/tfa_cso/electronv/idle2.wav" )
TFA.AddWeaponSound( "ElectronV.Idle3", "weapons/tfa_cso/electronv/idle3.wav" )
TFA.AddWeaponSound( "ElectronV.ClipIn", "weapons/tfa_cso/electronv/clipin.wav" )
TFA.AddWeaponSound( "ElectronV.ClipOut", "weapons/tfa_cso/electronv/clipout.wav" )

--Sepctre M4
TFA.AddFireSound( "Spectre.Fire", "weapons/tfa_cso/spectre/fire.wav", false, "^" )
TFA.AddWeaponSound ( "Spectre.ClipOut", "weapons/tfa_cso/spectre/clipout.wav" )
TFA.AddWeaponSound ( "Spectre.ClipIn", "weapons/tfa_cso/spectre/clipin.wav" )
TFA.AddWeaponSound ( "Spectre.Foley1", "weapons/tfa_cso/spectre/foley1.wav" )
TFA.AddWeaponSound ( "Spectre.Draw", "weapons/tfa_cso/spectre/draw.wav" )

--Lightning Fury
TFA.AddFireSound( "Jetgun.Fire", "weapons/tfa_cso/jetgun/fire.wav", false, "^" )
TFA.AddWeaponSound( "Jetgun.Fire_End", "weapons/tfa_cso/jetgun/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "Jetgun.Dash", "weapons/tfa_cso/jetgun/dash.wav", false, "^" )
TFA.AddWeaponSound( "Jetgun.Idle", "weapons/tfa_cso/jetgun/idle.wav" )
TFA.AddWeaponSound( "Jetgun.Draw", "weapons/tfa_cso/jetgun/draw.wav" )
TFA.AddWeaponSound( "Jetgun.Reload", "weapons/tfa_cso/jetgun/reload.wav" )
TFA.AddWeaponSound( "Jetgun.Beep", "weapons/tfa_cso/jetgun/beep.wav" )

--Bear Buster
TFA.AddFireSound( "BearBuster.Fire", "weapons/tfa_cso/bearbuster/fire.wav", false, "^" )
TFA.AddWeaponSound( "BearBuster.Fire2", "weapons/tfa_cso/bearbuster/fire2.wav", false, "^" )
TFA.AddWeaponSound( "BearBuster.Draw", "weapons/tfa_cso/bearbuster/draw.wav" )
TFA.AddWeaponSound( "BearBuster.Reload", "weapons/tfa_cso/bearbuster/reload.wav" )
TFA.AddWeaponSound( "BearBuster.Beep", "weapons/tfa_cso/bearbuster/beep.wav" )

--Bear Fury MK-1
TFA.AddFireSound( "BearFuryMK1.Fire", "weapons/tfa_cso/bearfurymk1/fire.wav", false, "^" )
TFA.AddWeaponSound( "BearFuryMK1.Dash", "weapons/tfa_cso/bearfurymk1/dash.wav", false, "^" )
TFA.AddWeaponSound( "BearFuryMK1.Draw", "weapons/tfa_cso/bearfurymk1/draw.wav" )
TFA.AddWeaponSound( "BearFuryMK1.Reload", "weapons/tfa_cso/bearfurymk1/reload.wav" )

--Bear Fury MK-2
TFA.AddWeaponSound( "BearFuryMK2.Dash", "weapons/tfa_cso/bearfurymk2/dash.wav", false, "^" )
TFA.AddWeaponSound( "BearFuryMK2.Draw", "weapons/tfa_cso/bearfurymk2/draw.wav" )
TFA.AddWeaponSound( "BearFuryMK2.Reload", "weapons/tfa_cso/bearfurymk2/reload.wav" )

--Bear Fury MK-3
TFA.AddFireSound( "BearFuryMK3.Fire", "weapons/tfa_cso/bearfurymk3/fire.wav", false, "^" )
TFA.AddWeaponSound( "BearFuryMK3.Fire_End", "weapons/tfa_cso/bearfurymk3/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "BearFuryMK3.Dash", "weapons/tfa_cso/bearfurymk3/dash.wav", false, "^" )
TFA.AddWeaponSound( "BearFuryMK3.Draw", "weapons/tfa_cso/bearfurymk3/draw.wav" )
TFA.AddWeaponSound( "BearFuryMK3.Reload", "weapons/tfa_cso/bearfurymk3/reload.wav" )

--Twin Hawk
TFA.AddFireSound( "RocketPistol.Fire_Loop", "weapons/tfa_cso/rocketpistol/fire_loop.wav", false, "^" )
TFA.AddWeaponSound( "RocketPistol.Fire_End", "weapons/tfa_cso/rocketpistol/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "RocketPistol.Fire2", "weapons/tfa_cso/rocketpistol/fire2.wav", false, "^" )
TFA.AddWeaponSound( "RocketPistol.Draw", "weapons/tfa_cso/rocketpistol/draw.wav" )
TFA.AddWeaponSound( "RocketPistol.Reload", "weapons/tfa_cso/rocketpistol/reload.wav" )
TFA.AddWeaponSound( "RocketPistol.Fire1", "weapons/tfa_cso/rocketpistol/fire1.wav" )

--SKULL-7
TFA.AddFireSound( "M249EX.Fire_Old", "weapons/tfa_cso/m249ex/fire_old.wav", false, "^" )

--Gravity Repulsor
TFA.AddFireSound( "Leapstrikegun.Fire", "weapons/tfa_cso/leapstrikegun/fire.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Exp", "weapons/tfa_cso/leapstrikegun/exp.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Idle", "weapons/tfa_cso/leapstrikegun/idle.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Draw", "weapons/tfa_cso/leapstrikegun/draw.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.ClipIn", "weapons/tfa_cso/leapstrikegun/clipin.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.ClipOut", "weapons/tfa_cso/leapstrikegun/clipout.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_End", "weapons/tfa_cso/leapstrikegun/change_end.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_Fly", "weapons/tfa_cso/leapstrikegun/change_fly.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_Fly_Idle", "weapons/tfa_cso/leapstrikegun/change_fly_idle.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_Fly_Exp", "weapons/tfa_cso/leapstrikegun/change_fly_exp.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_Idle", "weapons/tfa_cso/leapstrikegun/change_idle.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_Star_Fx", "weapons/tfa_cso/leapstrikegun/change_star_fx.wav", false, "^" )
TFA.AddWeaponSound( "Leapstrikegun.Change_Start", "weapons/tfa_cso/leapstrikegun/change_start.wav", false, "^" )

--Sylphid
TFA.AddFireSound( "Sylphid.Fire", "weapons/tfa_cso/sylphid/fire.wav", false, "^" )
TFA.AddFireSound( "Sylphid.Fire2", "weapons/tfa_cso/sylphid/fire2.wav", false, "^" )
TFA.AddFireSound( "Sylphid.Fire3", "weapons/tfa_cso/sylphid/fire3.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Draw", "weapons/tfa_cso/sylphid/draw.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Idle", "weapons/tfa_cso/sylphid/idle.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Reload", "weapons/tfa_cso/sylphid/reload.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Shoot", "weapons/tfa_cso/sylphid/shoot.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Spin", "weapons/tfa_cso/sylphid/spin.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Ready", "weapons/tfa_cso/sylphid/ready.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Jump_Spin", "weapons/tfa_cso/sylphid/jump_spin.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Jump1", "weapons/tfa_cso/sylphid/jump1.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Jump2", "weapons/tfa_cso/sylphid/jump2.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Bird_Idle", "weapons/tfa_cso/sylphid/bird_idle.wav", false, "^" )
TFA.AddWeaponSound( "Sylphid.Bird_Shoot", "weapons/tfa_cso/sylphid/bird_shoot.wav", false, "^" )