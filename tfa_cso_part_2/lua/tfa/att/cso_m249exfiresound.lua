if not ATTACHMENT then
	ATTACHMENT = {}
end

ATTACHMENT.Name = "Fire Sound" -- skin name
ATTACHMENT.Description = {"Sets default fire sound from all regions"}
ATTACHMENT.Icon = "entities/tfa_cso_icon.png" -- icon
ATTACHMENT.ShortName = "Fire Sound" -- short name that displayed inside the icon

ATTACHMENT.WeaponTable = {
	["Primary.Sound"] = Sound("M249EX.Fire_Old"),
}

if not TFA_ATTACHMENT_ISUPDATING then
	TFAUpdateAttachments()
end
