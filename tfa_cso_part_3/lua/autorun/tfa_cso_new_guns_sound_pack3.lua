--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--Galil maverick
TFA.AddFireSound( "GalilCraft.Fire", "weapons/tfa_cso/galilcraft/fire.wav", false, "^" )
TFA.AddWeaponSound( "GalilCraft.ClipIn", "weapons/tfa_cso/galilcraft/clipin.wav")
TFA.AddWeaponSound( "GalilCraft.ClipOut", "weapons/tfa_cso/galilcraft/clipout.wav")
TFA.AddWeaponSound( "GalilCraft.Boltpull", "weapons/tfa_cso/galilcraft/boltpull.wav")

--Hunter Killer X-12
TFA.AddFireSound( "X-12.Fire", "weapons/tfa_cso/x-12/fire.wav", false, "^" )
TFA.AddWeaponSound( "X-12.Draw", "weapons/tfa_cso/x-12/draw.wav" )
TFA.AddWeaponSound( "X-12.ClipIn1", "weapons/tfa_cso/x-12/clipin1.wav" )
TFA.AddWeaponSound( "X-12.ClipIn2", "weapons/tfa_cso/x-12/clipin2.wav" )
TFA.AddWeaponSound( "X-12.ClipOut1", "weapons/tfa_cso/x-12/clipout1.wav" )
TFA.AddWeaponSound( "X-12.ClipOut2", "weapons/tfa_cso/x-12/clipout2.wav" )

--Voxel Minigun
TFA.AddFireSound( "Voxelminigun.Shoot", "weapons/tfa_cso/voxelminigun/shoot.wav", false, "^" )
TFA.AddWeaponSound( "Voxelminigun.Draw", "weapons/tfa_cso/voxelminigun/draw.wav" )
TFA.AddWeaponSound( "Voxelminigun.Change", "weapons/tfa_cso/voxelminigun/change.wav" )

--Colt King Cobra
TFA.AddFireSound( "KingCobra.Fire", "weapons/tfa_cso/kingcobra/fire.wav", false, "^" )
TFA.AddWeaponSound( "KingCobra.Draw", "weapons/tfa_cso/kingcobra/draw.wav" )
TFA.AddWeaponSound( "KingCobra.ClipIn", "weapons/tfa_cso/kingcobra/clipin.wav" )
TFA.AddWeaponSound( "KingCobra.ClipOut", "weapons/tfa_cso/kingcobra/clipout.wav" )

--XM1014
TFA.AddFireSound( "XM1014.Fire", "weapons/tfa_cso/xm1014/fire.wav", false, "^" )
TFA.AddWeaponSound( "XM1014.Insert", "weapons/tfa_cso/xm1014/insert.wav" )

--Vulcanus-5
TFA.AddFireSound( "Vulcanus5.Fire", "weapons/tfa_cso/vulcanus5/fire.wav", false, "^" )
TFA.AddWeaponSound( "Vulcanus5.Boltpull", "weapons/tfa_cso/vulcanus5/boltpull.wav" )
TFA.AddWeaponSound( "Vulcanus5.ClipIn", "weapons/tfa_cso/vulcanus5/clipin.wav" )
TFA.AddWeaponSound( "Vulcanus5.ClipOut", "weapons/tfa_cso/vulcanus5/clipout.wav" )

--Vulcanus-11
TFA.AddFireSound( "Vulcanus11.Fire", "weapons/tfa_cso/vulcanus11/fire.wav", false, "^" )
TFA.AddFireSound( "Vulcanus11.Fire2", "weapons/tfa_cso/vulcanus11/fire2.wav", false, "^" )
TFA.AddWeaponSound( "Vulcanus11.Draw", "weapons/tfa_cso/vulcanus11/draw.wav" )
TFA.AddWeaponSound( "Vulcanus11.Reload", "weapons/tfa_cso/vulcanus11/reload.wav" )
TFA.AddWeaponSound( "Vulcanus11.Insert", "weapons/tfa_cso/vulcanus11/insert.wav" )
TFA.AddWeaponSound( "Vulcanus11.Change1", "weapons/tfa_cso/vulcanus11/change1.wav" )
TFA.AddWeaponSound( "Vulcanus11.Change2", "weapons/tfa_cso/vulcanus11/change2.wav" )

--M1887-Xmas
TFA.AddFireSound( "M1887XMAS.Fire", "weapons/tfa_cso/m1887xmas/fire.wav", false, "^" )
TFA.AddWeaponSound( "M1887XMAS.Draw", "weapons/tfa_cso/m1887xmas/draw.wav" )
TFA.AddWeaponSound( "M1887XMAS.Start", "weapons/tfa_cso/m1887xmas/start.wav" )
TFA.AddWeaponSound( "M1887XMAS.Insert", "weapons/tfa_cso/m1887xmas/insert.wav" )
TFA.AddWeaponSound( "M1887XMAS.Finish", "weapons/tfa_cso/m1887xmas/finish.wav" )

--RPG-7
TFA.AddFireSound( "RPG7.Fire", "weapons/tfa_cso/rpg7/fire.wav", false, "^" )
TFA.AddWeaponSound( "RPG7.Draw", "weapons/tfa_cso/rpg7/draw.wav" )
TFA.AddWeaponSound( "RPG7.Reload", "weapons/tfa_cso/rpg7/reload.wav" )

--AT4
TFA.AddFireSound( "AT4.Fire", "weapons/tfa_cso/at4/fire.wav", false, "^" )
TFA.AddWeaponSound( "AT4.Draw", "weapons/tfa_cso/at4/draw.wav" )
TFA.AddWeaponSound( "AT4.ClipIn1", "weapons/tfa_cso/at4/clipin1.wav" )
TFA.AddWeaponSound( "AT4.ClipIn2", "weapons/tfa_cso/at4/clipin2.wav" )
TFA.AddWeaponSound( "AT4.ClipIn3", "weapons/tfa_cso/at4/clipin3.wav" )

--UTS-15
TFA.AddFireSound( "UTS15.Fire", "weapons/tfa_cso/uts15/fire.wav", false, "^" )
TFA.AddWeaponSound( "UTS15.Draw", "weapons/tfa_cso/uts15/draw.wav" )
TFA.AddWeaponSound( "UTS15.Reload", "weapons/tfa_cso/uts15/reload.wav" )
TFA.AddWeaponSound( "UTS15.Insert", "weapons/tfa_cso/uts15/insert.wav" )

--Cyclone
TFA.AddFireSound( "Cyclone.Fire", "weapons/tfa_cso/cyclone/fire.wav", false, "^" )
TFA.AddFireSound( "Cyclone.Fire_End", "weapons/tfa_cso/cyclone/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "Cyclone.Draw", "weapons/tfa_cso/cyclone/draw.wav" )
TFA.AddWeaponSound( "Cyclone.ClipIn", "weapons/tfa_cso/cyclone/clipin.wav" )
TFA.AddWeaponSound( "Cyclone.ClipOut", "weapons/tfa_cso/cyclone/clipout.wav" )
TFA.AddWeaponSound( "Cyclone.Idle", "weapons/tfa_cso/cyclone/idle.wav" )

--Tornado
TFA.AddFireSound( "Tornado.Fire1", "weapons/tfa_cso/tornado/fire1.wav", false, "^" )
TFA.AddFireSound( "Tornado.Fire2", "weapons/tfa_cso/tornado/fire2.wav", false, "^" )
TFA.AddFireSound( "Tornado.Fire3", "weapons/tfa_cso/tornado/fire3.wav", false, "^" )
TFA.AddFireSound( "Tornado.Fire_End", "weapons/tfa_cso/tornado/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "Tornado.DrawA", "weapons/tfa_cso/tornado/draw_a.wav" )
TFA.AddWeaponSound( "Tornado.DrawC", "weapons/tfa_cso/tornado/draw_c.wav" )
TFA.AddWeaponSound( "Tornado.ClipIn", "weapons/tfa_cso/tornado/clipin.wav" )
TFA.AddWeaponSound( "Tornado.ClipOut", "weapons/tfa_cso/tornado/clipout.wav" )

--Lightning SG-1
TFA.AddFireSound( "Umbrella.Fire", "weapons/tfa_cso/umbrella/fire.wav", false, "^" )
TFA.AddWeaponSound( "Umbrella.Draw", "weapons/tfa_cso/umbrella/draw.wav" )
TFA.AddWeaponSound( "Umbrella.Start", "weapons/tfa_cso/umbrella/start.wav" )
TFA.AddWeaponSound( "Umbrella.Insert", "weapons/tfa_cso/umbrella/insert.wav" )
TFA.AddWeaponSound( "Umbrella.Finish", "weapons/tfa_cso/umbrella/finish.wav" )

--CHARGER-5
TFA.AddFireSound( "Charger5.Fire", "weapons/tfa_cso/charger5/fire.wav", false, "^" )
TFA.AddWeaponSound( "Charger5.ClipOut", "weapons/tfa_cso/charger5/clipout.wav" )
TFA.AddWeaponSound( "Charger5.ClipIn1", "weapons/tfa_cso/charger5/clipin1.wav" )
TFA.AddWeaponSound( "Charger5.ClipIn2", "weapons/tfa_cso/charger5/clipin2.wav" )

--JANUS-11
TFA.AddFireSound( "JANUS11.Fire1", "weapons/tfa_cso/janus11/fire1.wav", false, "^" )
TFA.AddFireSound( "JANUS11.Fire2", "weapons/tfa_cso/janus11/fire2.wav", false, "^" )
TFA.AddWeaponSound( "JANUS11.Draw", "weapons/tfa_cso/janus11/draw.wav" )
TFA.AddWeaponSound( "JANUS11.Insert", "weapons/tfa_cso/janus11/insert.wav" )
TFA.AddWeaponSound( "JANUS11.Finish", "weapons/tfa_cso/janus11/finish.wav" )
TFA.AddWeaponSound( "JANUS11.Change1", "weapons/tfa_cso/janus11/change1.wav" )
TFA.AddWeaponSound( "JANUS11.Change2", "weapons/tfa_cso/janus11/change2.wav" )

--M95 Tiger
TFA.AddFireSound( "M95Tiger.Fire", "weapons/tfa_cso/m95tiger/fire.wav", false, "^" )
TFA.AddFireSound( "M95Tiger.Fire2", "weapons/tfa_cso/m95tiger/fire_2.wav", false, "^" )
TFA.AddWeaponSound( "M95Tiger.Draw", "weapons/tfa_cso/m95tiger/draw.wav" )
--TFA.AddWeaponSound( "M95Tiger.Idle", "weapons/tfa_cso/m95tiger/idle.wav" ) lets the idle sound play over on itself, so...
local soundData = {
	name 		= "M95Tiger.Idle" ,
	channel 	= CHAN_WEAPON,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/m95tiger/idle.wav"
}

sound.Add(soundData)
TFA.AddWeaponSound( "M95Tiger.ClipOut", "weapons/tfa_cso/m95tiger/clipout.wav" )
TFA.AddWeaponSound( "M95Tiger.ClipIn", "weapons/tfa_cso/m95tiger/clipin.wav" )
TFA.AddWeaponSound( "M95Tiger.Point", "weapons/tfa_cso/m95tiger/point.wav" )
TFA.AddWeaponSound( "M95Tiger.Net1", "weapons/tfa_cso/m95tiger/net1.wav" )
TFA.AddWeaponSound( "M95Tiger.Net2", "weapons/tfa_cso/m95tiger/net2.wav" )

--TANK
TFA.AddFireSound( "TANK.Fire", "weapons/tfa_cso/tank/fire.wav", false, "^" )
TFA.AddFireSound( "TANK.Fire_Empty", "weapons/tfa_cso/tank/fire_empty.wav", false, "^" )
TFA.AddWeaponSound( "TANK.Draw", "weapons/tfa_cso/tank/draw.wav" )
TFA.AddWeaponSound( "TANK.ChangeA", "weapons/tfa_cso/tank/changea.wav" )
TFA.AddWeaponSound( "TANK.changeB", "weapons/tfa_cso/tank/changeb.wav" )
TFA.AddWeaponSound( "TANK.Idle", "weapons/tfa_cso/tank/idle.wav" )
TFA.AddWeaponSound( "TANK.Idle2", "weapons/tfa_cso/tank/idle2.wav" )

--KATYUSHA
TFA.AddFireSound( "KATYUSHA.Fire", "weapons/tfa_cso/katyusha/fire.wav", false, "^" )
TFA.AddFireSound( "KATYUSHA.Reload", "weapons/tfa_cso/katyusha/reload.wav", false, "^" )
TFA.AddWeaponSound( "KATYUSHA.Draw", "weapons/tfa_cso/katyusha/draw.wav" )
TFA.AddWeaponSound( "KATYUSHA.ChangeA", "weapons/tfa_cso/katyusha/changea.wav" )
TFA.AddWeaponSound( "KATYUSHA.ChangeB", "weapons/tfa_cso/katyusha/changeb.wav" )
TFA.AddWeaponSound( "KATYUSHA.Idle", "weapons/tfa_cso/katyusha/idle.wav" )
TFA.AddWeaponSound( "KATYUSHA.Idle2", "weapons/tfa_cso/katyusha/idle2.wav" )

--HOWITZER
TFA.AddFireSound( "HOWITZER.Fire", "weapons/tfa_cso/howitzer/fire.wav", false, "^" )
TFA.AddFireSound( "HOWITZER.Reload", "weapons/tfa_cso/howitzer/reload.wav", false, "^" )
TFA.AddWeaponSound( "HOWITZER.Draw", "weapons/tfa_cso/howitzer/draw.wav" )
TFA.AddWeaponSound( "HOWITZER.ChangeA", "weapons/tfa_cso/howitzer/changea.wav" )
TFA.AddWeaponSound( "HOWITZER.ChangeB", "weapons/tfa_cso/howitzer/changeb.wav" )
TFA.AddWeaponSound( "HOWITZER.Idle", "weapons/tfa_cso/howitzer/idle.wav" )
TFA.AddWeaponSound( "HOWITZER.Fire_start", "weapons/tfa_cso/howitzer/fire_start.wav" )

--V2ROCKET
TFA.AddFireSound( "V2ROCKET.Fire", "weapons/tfa_cso/v2rocket/fire.wav", false, "^" )
TFA.AddFireSound( "V2ROCKET.Reload", "weapons/tfa_cso/v2rocket/reload.wav", false, "^" )
TFA.AddWeaponSound( "V2ROCKET.Draw", "weapons/tfa_cso/v2rocket/draw.wav" )
TFA.AddWeaponSound( "V2ROCKET.ChangeA", "weapons/tfa_cso/v2rocket/changea.wav" )
TFA.AddWeaponSound( "V2ROCKET.ChangeB", "weapons/tfa_cso/v2rocket/changeb.wav" )
TFA.AddWeaponSound( "V2ROCKET.ChangeB_Empty", "weapons/tfa_cso/v2rocket/changeb_empty.wav" )
TFA.AddWeaponSound( "V2ROCKET.Idle", "weapons/tfa_cso/v2rocket/idle.wav" )
TFA.AddWeaponSound( "V2ROCKET.Fire_Start", "weapons/tfa_cso/v2rocket/fire_start.wav" )
TFA.AddWeaponSound( "V2ROCKET.Fire_End", "weapons/tfa_cso/v2rocket/fire_end.wav" )

--CROW-3
TFA.AddFireSound( "CROW3.Fire", "weapons/tfa_cso/crow3/fire.wav", false, "^" )
TFA.AddWeaponSound( "CROW3.Draw", "weapons/tfa_cso/crow3/draw.wav" )
TFA.AddWeaponSound( "CROW3.Reload_In", "weapons/tfa_cso/crow3/reload_in.wav" )
TFA.AddWeaponSound( "CROW3.Reload_A", "weapons/tfa_cso/crow3/reload_a.wav" )
TFA.AddWeaponSound( "CROW3.Reload_B", "weapons/tfa_cso/crow3/reload_b.wav" )
TFA.AddWeaponSound( "CROW3.Reload_Boltpull", "weapons/tfa_cso/crow3/reload_boltpull.wav" )

--CROW-7
TFA.AddFireSound( "CROW7.Fire", "weapons/tfa_cso/crow7/fire.wav", false, "^" )
TFA.AddWeaponSound( "CROW7.Beep", "weapons/tfa_cso/crow7/beep.wav" )
TFA.AddWeaponSound( "CROW7.Draw", "weapons/tfa_cso/crow7/draw.wav" )
TFA.AddWeaponSound( "CROW7.Reload_In", "weapons/tfa_cso/crow7/reload_in.wav" )
TFA.AddWeaponSound( "CROW7.Reload_A", "weapons/tfa_cso/crow7/reloada.wav" )
TFA.AddWeaponSound( "CROW7.Reload_B_Clipout", "weapons/tfa_cso/crow7/reloadb_clipout.wav" )
TFA.AddWeaponSound( "CROW7.Reload_B_Clipin", "weapons/tfa_cso/crow7/reloadb_clipin.wav" )
TFA.AddWeaponSound( "CROW7.Reload_B_Boltpull", "weapons/tfa_cso/crow7/reloadb_boltpull.wav" )

--CROW-11
TFA.AddFireSound( "CROW11.Fire", "weapons/tfa_cso/crow11/fire.wav", false, "^" )
TFA.AddWeaponSound( "CROW11.Draw", "weapons/tfa_cso/crow11/draw.wav" )
TFA.AddWeaponSound( "CROW11.Reload_In", "weapons/tfa_cso/crow11/reload_in.wav" )
TFA.AddWeaponSound( "CROW11.Reload_A", "weapons/tfa_cso/crow11/reload_a.wav" )
TFA.AddWeaponSound( "CROW11.Reload_B1", "weapons/tfa_cso/crow11/reload_b1.wav" )
TFA.AddWeaponSound( "CROW11.Reload_B2", "weapons/tfa_cso/crow11/reload_b2.wav" )

--Vulcanus-7
TFA.AddFireSound( "Vulcanus7.Fire", "weapons/tfa_cso/vulcanus7/fire.wav", false, "^" )
TFA.AddFireSound( "Vulcanus7.Fire2", "weapons/tfa_cso/vulcanus7/fire2.wav", false, "^" )
TFA.AddFireSound( "Vulcanus7.FireB", "weapons/tfa_cso/vulcanus7/fire_b.wav", false, "^" )
TFA.AddWeaponSound( "Vulcanus7.ClipOn", "weapons/tfa_cso/vulcanus7/clipon.wav" )
TFA.AddWeaponSound( "Vulcanus7.ClipIn", "weapons/tfa_cso/vulcanus7/clipin.wav" )
TFA.AddWeaponSound( "Vulcanus7.ClipOut", "weapons/tfa_cso/vulcanus7/clipout.wav" )
TFA.AddWeaponSound( "Vulcanus7.Change", "weapons/tfa_cso/vulcanus7/change.wav" )

--Gunkata
TFA.AddFireSound( "Gunkata.Fire", "weapons/tfa_cso/gunkata/fire.wav", false, "^" )
TFA.AddWeaponSound( "Gunkata.Reload", "weapons/tfa_cso/gunkata/reload.wav" )
TFA.AddWeaponSound( "Gunkata.Reload2", "weapons/tfa_cso/gunkata/reload2.wav" )
TFA.AddWeaponSound( "Gunkata.Draw2", "weapons/tfa_cso/gunkata/draw2.wav" )
TFA.AddWeaponSound( "Gunkata.Draw", "weapons/tfa_cso/gunkata/draw.wav" )
TFA.AddWeaponSound( "Gunkata.Idle", "weapons/tfa_cso/gunkata/idle.wav" )

--Drill Gun
TFA.AddFireSound( "Drillgun.Fire", "weapons/tfa_cso/drillgun/fire.wav", false, "^" )
TFA.AddWeaponSound( "Drillgun.Reload", "weapons/tfa_cso/drillgun/reload.wav" )
TFA.AddWeaponSound( "Drillgun.Draw", "weapons/tfa_cso/drillgun/draw.wav" )

--Plasma Grenade
TFA.AddFireSound( "Sfnade.Boom", "weapons/tfa_cso/sfgrenade/boom.wav", false, "^" )
TFA.AddWeaponSound( "Sfnade.Pullpin", "weapons/tfa_cso/sfgrenade/pullpin.wav" )
TFA.AddWeaponSound( "Sfnade.Draw", "weapons/tfa_cso/sfgrenade/draw.wav" )
TFA.AddWeaponSound( "Sfnade.Ready", "weapons/tfa_cso/sfgrenade/ready.wav" )

--Dao Grenade
TFA.AddWeaponSound( "CartFrag.Pullpin", "weapons/tfa_cso/cartfrag/cartfrag.wav" )

--Frag Grenade
TFA.AddWeaponSound( "Fragnade.Pullpin", "weapons/tfa_cso/fragnade/pinpull.wav" )

--Pumpkin
TFA.AddFireSound( "Pumpkin.Boom", "weapons/tfa_cso/pumpkin/boom.wav", false, "^" )
TFA.AddWeaponSound( "Pumpkin.Pullpin", "weapons/tfa_cso/pumpkin/pullpin.wav" )
TFA.AddWeaponSound( "Pumpkin.Draw", "weapons/tfa_cso/pumpkin/draw.wav" )
TFA.AddWeaponSound( "Pumpkin.Throw", "weapons/tfa_cso/pumpkin/throw.wav" )

--Mooncake
TFA.AddFireSound( "Mooncake.Boom", "weapons/tfa_cso/mooncake/boom.wav", false, "^" )
TFA.AddWeaponSound( "Mooncake.Pullpin1", "weapons/tfa_cso/mooncake/pullpin1.wav" )
TFA.AddWeaponSound( "Mooncake.Pullpin2", "weapons/tfa_cso/mooncake/pullpin2.wav" )

--Gungnir
TFA.AddFireSound( "Gungnir.Shoot_Exp", "weapons/tfa_cso/gungnir/charge_shoot_exp.wav", false, "^" )
TFA.AddFireSound( "Gungnir.Shoot_1", "weapons/tfa_cso/gungnir/charge_shoot1.wav", false, "^" )
TFA.AddFireSound( "Gungnir.Shoot_B", "weapons/tfa_cso/gungnir/shoot_b.wav", false, "^" )
TFA.AddFireSound( "Gungnir.Shoot_B_exp", "weapons/tfa_cso/gungnir/shoot_b_exp.wav", false, "^" )
TFA.AddFireSound( "Gungnir.Shoot_Loop", "weapons/tfa_cso/gungnir/shoot_loop.wav", false, "^" )
TFA.AddFireSound( "Gungnir.End", "weapons/tfa_cso/gungnir/shoot_end.wav" )
TFA.AddWeaponSound( "Gungnir.Charge_loop", "weapons/tfa_cso/gungnir/charge_loop.wav" )
TFA.AddWeaponSound( "Gungnir.Charge_Shoot2", "weapons/tfa_cso/gungnir/charge_shoot2.wav" )
TFA.AddWeaponSound( "Gungnir.Shoot_B_Charge", "weapons/tfa_cso/gungnir/shoot_b_charge.wav" )
TFA.AddWeaponSound( "Gungnir.Draw", "weapons/tfa_cso/gungnir/draw.wav" )
TFA.AddWeaponSound( "Gungnir.Reload", "weapons/tfa_cso/gungnir/reload.wav" )
TFA.AddWeaponSound( "Gungnir.Idle", "weapons/tfa_cso/gungnir/idle.wav" )

--Thunder Pistol
TFA.AddFireSound( "Thunder.Shoot1", "weapons/tfa_cso/thunderpistol/shoot1.wav", false, "^" )
TFA.AddFireSound( "Thunder.Shoot", "weapons/tfa_cso/thunderpistol/shoot.wav", false, "^" )
TFA.AddFireSound( "Thunder.Exp", "weapons/tfa_cso/thunderpistol/exp.wav", false, "^" )
TFA.AddFireSound( "Thunder.Bigger_Exp", "weapons/tfa_cso/thunderpistol/bigger_exp.wav" )
TFA.AddWeaponSound( "Thunder.ClipOut", "weapons/tfa_cso/thunderpistol/clipout.wav" )
TFA.AddWeaponSound( "Thunder.Draw", "weapons/tfa_cso/thunderpistol/draw.wav" )
TFA.AddWeaponSound( "Thunder.Cloak", "weapons/tfa_cso/thunderpistol/cloak.wav" )
TFA.AddWeaponSound( "Thunder.ClipIn", "weapons/tfa_cso/thunderpistol/clipin.wav" )
TFA.AddWeaponSound( "Thunder.Idle1", "weapons/tfa_cso/thunderpistol/idle1.wav" )
TFA.AddWeaponSound( "Thunder.Idle2", "weapons/tfa_cso/thunderpistol/idle2.wav" )
TFA.AddWeaponSound( "Thunder.Idle3", "weapons/tfa_cso/thunderpistol/idle3.wav" )
TFA.AddWeaponSound( "Thunder.Loop", "weapons/tfa_cso/thunderpistol/idle_loop.wav" )

--Compound Bow
TFA.AddFireSound( "Bow.Shoot", "weapons/tfa_cso/bow/shoot.wav", false, "^" )
TFA.AddFireSound( "Bow.Charge_Shoot", "weapons/tfa_cso/bow/charge_shoot.wav", false, "^" )
TFA.AddWeaponSound( "Bow.ChargeStart1", "weapons/tfa_cso/bow/charge_start1.wav" )
TFA.AddWeaponSound( "Bow.ChargeStart2", "weapons/tfa_cso/bow/charge_start2.wav" )
TFA.AddWeaponSound( "Bow.ChargeFinish", "weapons/tfa_cso/bow/charge_finish.wav" )
TFA.AddWeaponSound( "Bow.Draw", "weapons/tfa_cso/bow/draw.wav" )

--Triple Tactical Knife
TFA.AddFireSound( "Tknife.Shoot", "weapons/tfa_cso/tknife/tknife_shoot1.wav", false, "^" )
TFA.AddFireSound( "Tknife.Shoot2", "weapons/tfa_cso/tknife/tknife_shoot2.wav", false, "^" )
TFA.AddFireSound( "Tknifeex.Shoot2", "weapons/tfa_cso/tknife/tknifeex_shoot2.wav", false, "^" )
TFA.AddWeaponSound( "Tknife.Draw", "weapons/tfa_cso/tknife/tknife_draw.wav" )
TFA.AddWeaponSound( "Tknifeex.Draw", "weapons/tfa_cso/tknife/tknifeex_draw.wav" )
TFA.AddWeaponSound( "Tknifeex2.Draw", "weapons/tfa_cso/tknife/tknifeex2_draw.wav" )
TFA.AddWeaponSound( "Tknife.Hitwall", "weapons/tfa_cso/tknife/hitwall.wav" )
TFA.AddWeaponSound( "Tknife.Hitwall2", "weapons/tfa_cso/tknife/hitwall2.wav" )

--Stinger
TFA.AddFireSound( "Stinger.Fire", "weapons/tfa_cso/stinger/fire.wav", false, "^" )
TFA.AddFireSound( "Stinger.Empty", "weapons/tfa_cso/stinger/empty.wav", false, "^" )
TFA.AddWeaponSound( "Stinger.Reload1", "weapons/tfa_cso/stinger/reload1.wav" )
TFA.AddWeaponSound( "Stinger.Reload2", "weapons/tfa_cso/stinger/reload2.wav" )
TFA.AddWeaponSound( "Stinger.Draw_Empty", "weapons/tfa_cso/stinger/draw_empty.wav" )
TFA.AddWeaponSound( "Stinger.Draw", "weapons/tfa_cso/stinger/draw.wav" )

--Speargun
TFA.AddFireSound( "Speargun.Fire", "weapons/tfa_cso/speargun/fire.wav")
TFA.AddWeaponSound( "Speargun.ClipIn", "weapons/tfa_cso/speargun/clipin.wav" )
TFA.AddWeaponSound( "Speargun.Draw", "weapons/tfa_cso/speargun/draw.wav" )
TFA.AddWeaponSound( "Speargun.Stone1", "weapons/tfa_cso/speargun/stone1.wav" )
TFA.AddWeaponSound( "Speargun.Stone2", "weapons/tfa_cso/stinger/stone2.wav" )

--Needler
TFA.AddFireSound( "Needler.Fire", "weapons/tfa_cso/needler/fire.wav")
TFA.AddWeaponSound( "Needler.Clipin1", "weapons/tfa_cso/needler/clipin1.wav" )
TFA.AddWeaponSound( "Needler.Clipin2", "weapons/tfa_cso/needler/clipin2.wav" )
TFA.AddWeaponSound( "Needler.Clipout", "weapons/tfa_cso/needler/clipout.wav" )

--UMP45
TFA.AddFireSound( "Ump45.Fire", "weapons/tfa_cso/ump45/fire.wav", false, "^" )
TFA.AddWeaponSound( "Ump45.ClipIn", "weapons/tfa_cso/ump45/clipin.wav")
TFA.AddWeaponSound( "Ump45.ClipOut", "weapons/tfa_cso/ump45/clipout.wav")
TFA.AddWeaponSound( "UMP45.BoltPull", "weapons/tfa_cso/ump45/boltpull.wav")

--Famas
TFA.AddFireSound( "Famas.Fire", "weapons/tfa_cso/famas/fire1.wav", false, "^" )
TFA.AddWeaponSound( "Famas.Forearm", "weapons/tfa_cso/famas/forearm.wav" )
TFA.AddWeaponSound( "Famas.ClipIn", "weapons/tfa_cso/famas/clipin.wav" )
TFA.AddWeaponSound( "Famas.ClipOut", "weapons/tfa_cso/famas/clipout.wav" )

--Five-Seven
TFA.AddFireSound( "Fiveseven.Fire", "weapons/tfa_cso/fiveseven/fire.wav", false, "^" )
TFA.AddWeaponSound( "Fiveseven.ClipIn", "weapons/tfa_cso/fiveseven/clipin.wav")
TFA.AddWeaponSound( "Fiveseven.SlidePull", "weapons/tfa_cso/fiveseven/slidepull.wav")
TFA.AddWeaponSound( "Fiveseven.ClipOut", "weapons/tfa_cso/fiveseven/clipout.wav")
TFA.AddWeaponSound( "Fiveseven.SlideRelease", "weapons/tfa_cso/fiveseven/sliderelease.wav")

--Janus Ready

sound.Add({
	['name'] = "Janus.Ready",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/janus5/ready.wav"},
	['pitch'] = {100,100}
})

--Janus-7

TFA.AddFireSound( "Janus7.Fire1", "weapons/tfa_cso/janus7/fire1.wav", false, "^" )
TFA.AddFireSound( "Janus7.Fire2", "weapons/tfa_cso/janus7/fire2.wav", false, "^" )
TFA.AddWeaponSound( "Janus7.Open", "weapons/tfa_cso/janus7/open.wav")
TFA.AddWeaponSound( "Janus7.ClipOut", "weapons/tfa_cso/janus7/clipout.wav")
TFA.AddWeaponSound( "Janus7.ClipIn", "weapons/tfa_cso/janus7/clipin.wav")
TFA.AddWeaponSound( "Janus7.ClipLock", "weapons/tfa_cso/janus7/cliplock.wav")
TFA.AddWeaponSound( "Janus7.Close", "weapons/tfa_cso/janus7/close.wav")
TFA.AddWeaponSound( "Janus7.Change1", "weapons/tfa_cso/janus7/change1.wav")
TFA.AddWeaponSound( "Janus7.Change2", "weapons/tfa_cso/janus7/change2.wav")

--Janus-7 Xmas

TFA.AddFireSound( "Janus7.Fire1", "weapons/tfa_cso/janus7/fire1.wav", false, "^" )
TFA.AddFireSound( "Janus7.Fire2", "weapons/tfa_cso/janus7/fire2.wav", false, "^" )
TFA.AddWeaponSound( "Janus7.Open", "weapons/tfa_cso/janus7/open.wav")
TFA.AddWeaponSound( "Janus7Xmas.ClipOut1", "weapons/tfa_cso/janus7_xmas/clipout1.wav")
TFA.AddWeaponSound( "Janus7Xmas.ClipOut2", "weapons/tfa_cso/janus7_xmas/clipout2.wav")
TFA.AddWeaponSound( "Janus7Xmas.ClipIn1", "weapons/tfa_cso/janus7_xmas/clipin1.wav")
TFA.AddWeaponSound( "Janus7Xmas.ClipIn2", "weapons/tfa_cso/janus7_xmas/clipin2.wav")
TFA.AddWeaponSound( "Janus7Xmas.ClipIn3", "weapons/tfa_cso/janus7_xmas/clipin3.wav")
TFA.AddWeaponSound( "Janus7Xmas.Idle1", "weapons/tfa_cso/janus7_xmas/idle2_1.wav")
TFA.AddWeaponSound( "Janus7Xmas.Idle2", "weapons/tfa_cso/janus7_xmas/idle2_2.wav")
TFA.AddWeaponSound( "Janus7Xmas.Change1", "weapons/tfa_cso/janus7_xmas/change1.wav")
TFA.AddWeaponSound( "Janus7Xmas.Change2", "weapons/tfa_cso/janus7_xmas/change2.wav")

-- Doom Blaster

sound.Add({
	['name'] = "Doom_Blaster.Fire",
	['channel'] = CHAN_USER_BASE+11,
	['sound'] = { "weapons/tfa_cso/doom_blaster/fire.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Doom_Blaster.Fire2",
	['channel'] = CHAN_USER_BASE+11,
	['sound'] = { "weapons/tfa_cso/doom_blaster/fire2.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Doom_Blaster.Start",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/doom_blaster/start.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Doom_Blaster.Loop",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/doom_blaster/loop.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Doom_Blaster.Loop_End",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/doom_blaster/loop_end.wav"},
	['pitch'] = {100,100}
})

TFA.AddWeaponSound ( "Doom_Blaster.ClipOut", "weapons/tfa_cso/doom_blaster/clipout.wav" )
TFA.AddWeaponSound ( "Doom_Blaster.ClipIn", "weapons/tfa_cso/doom_blaster/clipin.wav" )
TFA.AddWeaponSound ( "Doom_Blaster.Draw", "weapons/tfa_cso/doom_blaster/draw.wav" )
TFA.AddWeaponSound ( "Doom_Blaster.WingStart", "weapons/tfa_cso/doom_blaster/wingstart.wav" )
TFA.AddWeaponSound ( "Doom_Blaster.WingEnd", "weapons/tfa_cso/doom_blaster/wingend.wav" )
TFA.AddWeaponSound ( "Doom_Blaster.Special", "weapons/tfa_cso/doom_blaster/special.wav" )

--Basketball
TFA.AddWeaponSound( "Basketball.PullPin", "weapons/tfa_cso/basketball/pullpin.wav" )
TFA.AddWeaponSound( "Basketball.Throw", "weapons/tfa_cso/basketball/throw.wav" )
TFA.AddWeaponSound( "Basketball.Draw", "weapons/tfa_cso/basketball/draw.wav" )
TFA.AddWeaponSound( "Basketball.Bounce1", "weapons/tfa_cso/basketball/bounce1.wav" )
TFA.AddWeaponSound( "Basketball.Bounce2", "weapons/tfa_cso/basketball/bounce2.wav" )

--Pierrot Magic Bow
sound.Add({
    ['name'] = "MagicBow.Fire",
    ['channel'] = CHAN_USER_BASE+11,
    ['sound'] = { "weapons/tfa_cso/magicbow/fire.wav"},
    ['pitch'] = {100,100}
})
sound.Add({
    ['name'] = "MagicBow.Fire2",
    ['channel'] = CHAN_USER_BASE+11,
    ['sound'] = { "weapons/tfa_cso/magicbow/fire2.wav"},
    ['pitch'] = {100,100}
})
sound.Add({
    ['name'] = "MagicBow.Exp",
    ['channel'] = CHAN_STATIC,
    ['sound'] = { "weapons/tfa_cso/magicbow/exp.wav"},
    ['pitch'] = {100,100}
})
sound.Add({
    ['name'] = "MagicBow.Exp2",
    ['channel'] = CHAN_STATIC,
    ['sound'] = { "weapons/tfa_cso/magicbow/exp2.wav"},
    ['pitch'] = {100,100}
})

TFA.AddWeaponSound ( "MagicBow.ClipOut1", "weapons/tfa_cso/magicbow/clipout1.wav" )
TFA.AddWeaponSound ( "MagicBow.ClipOut2", "weapons/tfa_cso/magicbow/clipout2.wav" )
TFA.AddWeaponSound ( "MagicBow.ClipIn1", "weapons/tfa_cso/magicbow/clipin1.wav" )
TFA.AddWeaponSound ( "MagicBow.ClipIn2", "weapons/tfa_cso/magicbow/clipin2.wav" )
TFA.AddWeaponSound ( "MagicBow.Draw", "weapons/tfa_cso/magicbow/draw.wav" )
TFA.AddWeaponSound ( "MagicBow.ZoomIn", "weapons/tfa_cso/magicbow/zoom_in.wav" )
TFA.AddWeaponSound ( "MagicBow.ZoomOut", "weapons/tfa_cso/magicbow/zoom_out.wav" )
TFA.AddWeaponSound ( "MagicBow.Zoom", "weapons/tfa_cso/magicbow/zoom.wav" )
TFA.AddWeaponSound ( "MagicBow.Beep", "weapons/tfa_cso/magicbow/beep.wav" )

--Sterling L2A3 Maverick

TFA.AddFireSound( "SterlingBayonet.Fire", "weapons/tfa_cso/sterlingbayonet/fire.wav", false, "^" )
TFA.AddWeaponSound( "SterlingBayonet.BoltPull", "weapons/tfa_cso/sterlingbayonet/boltpull.wav")
TFA.AddWeaponSound( "SterlingBayonet.ClipOut", "weapons/tfa_cso/sterlingbayonet/clipout.wav")
TFA.AddWeaponSound( "SterlingBayonet.ClipIn", "weapons/tfa_cso/sterlingbayonet/clipin.wav")
TFA.AddWeaponSound( "SterlingBayonet.Knife01", "weapons/tfa_cso/sterlingbayonet/knife01.wav")
TFA.AddWeaponSound( "SterlingBayonet.Knife02", "weapons/tfa_cso/sterlingbayonet/knife02.wav")
TFA.AddWeaponSound( "SterlingBayonet.Stone1", "weapons/tfa_cso/sterlingbayonet/stone1.wav")
TFA.AddWeaponSound( "SterlingBayonet.Stone2", "weapons/tfa_cso/sterlingbayonet/stone2.wav")
TFA.AddWeaponSound( "SterlingBayonet.Knife_Broken01", "weapons/tfa_cso/sterlingbayonet/knife_broken01.wav")
TFA.AddWeaponSound( "SterlingBayonet.Knife_Broken02", "weapons/tfa_cso/sterlingbayonet/knife_broken02.wav")
TFA.AddWeaponSound( "SterlingBayonet.Hit", "weapons/tfa_cso/sterlingbayonet/hit.wav")

--SG550
TFA.AddFireSound( "SG550.Fire", "weapons/tfa_cso/sg550/fire.wav", false, "^" )
TFA.AddWeaponSound( "SG550.Boltpull", "weapons/tfa_cso/sg550/boltpull.wav" )
TFA.AddWeaponSound( "SG550.ClipIn", "weapons/tfa_cso/sg550/clipin.wav" )
TFA.AddWeaponSound( "SG550.ClipOut", "weapons/tfa_cso/sg550/clipout.wav" )

--Galil
TFA.AddFireSound( "Galil.Fire", "weapons/tfa_cso/galil/fire.wav", false, "^" )
TFA.AddWeaponSound( "Galil.ClipOut", "weapons/tfa_cso/galil/clipout.wav" )
TFA.AddWeaponSound( "Galil.ClipIn", "weapons/tfa_cso/galil/clipin.wav" )
TFA.AddWeaponSound( "Falil.BoltPull", "weapons/tfa_cso/galil/boltpull.wav" )

--Elite
TFA.AddFireSound( "Elite.Fire", "weapons/tfa_cso/elite/fire.wav", false, "^" )
TFA.AddWeaponSound( "Elite.Draw", "weapons/tfa_cso/elite/draw.wav")
TFA.AddWeaponSound( "Elite.Clipout", "weapons/tfa_cso/elite/clipout.wav")
TFA.AddWeaponSound( "Elite.LeftClipin", "weapons/tfa_cso/elite/leftclipin.wav")
TFA.AddWeaponSound( "Elite.RightClipin", "weapons/tfa_cso/elite/rightclipin.wav")
TFA.AddWeaponSound( "Elite.SlideRelease", "weapons/tfa_cso/elite/sliderelease.wav")
TFA.AddWeaponSound( "Elite.ReloadStart", "weapons/tfa_cso/elite/twirl.wav")

--Dual Deagle
TFA.AddFireSound( "DDE.Fire", "weapons/tfa_cso/ddeagle/fire.wav", false, "^" )
TFA.AddWeaponSound( "DDE.ClipIn", "weapons/tfa_cso/ddeagle/clipin.wav")
TFA.AddWeaponSound( "DDE.ClipOut", "weapons/tfa_cso/ddeagle/clipout.wav")
TFA.AddWeaponSound( "DDE.ClipOff", "weapons/tfa_cso/ddeagle/clipoff.wav")
TFA.AddWeaponSound( "DDE.Twirl", "weapons/tfa_cso/ddeagle/twirl.wav")
TFA.AddWeaponSound( "DDE.Load", "weapons/tfa_cso/ddeagle/load.wav")

--K1A
TFA.AddFireSound( "K1A.Fire", "weapons/tfa_cso/k1a/fire.wav", false, "^" )
TFA.AddFireSound( "K1ASE.Fire", "weapons/tfa_cso/k1a/fire2.wav", false, "^" )
TFA.AddWeaponSound( "K1A.ClipOut", "weapons/tfa_cso/k1a/clipout.wav")
TFA.AddWeaponSound( "K1A.ClipIn", "weapons/tfa_cso/k1a/clipin.wav")
TFA.AddWeaponSound( "K1A.Foley1", "weapons/tfa_cso/k1a/foley1.wav")
TFA.AddWeaponSound( "K1A.Draw", "weapons/tfa_cso/k1a/draw.wav")

--SVD
TFA.AddFireSound( "SVD.Fire", "weapons/tfa_cso/svd/fire.wav", false, "^" )
TFA.AddWeaponSound( "SVD.ClipOut", "weapons/tfa_cso/svd/clipout.wav")
TFA.AddWeaponSound( "SVD.ClipIn", "weapons/tfa_cso/svd/clipin.wav")
TFA.AddWeaponSound( "SVD.Draw", "weapons/tfa_cso/svd/draw.wav")
TFA.AddWeaponSound( "SVD.ClipOn", "weapons/tfa_cso/svd/clipon.wav")

--QBB95
TFA.AddFireSound( "Qbb95.Fire", "weapons/tfa_cso/qbb95/fire.wav", false, "^" )
TFA.AddWeaponSound( "Qbb95.Draw", "weapons/tfa_cso/qbb95/draw.wav" )
TFA.AddWeaponSound( "Qbb95.ClipIn", "weapons/tfa_cso/qbb95/clipin.wav" )
TFA.AddWeaponSound( "Qbb95.ClipOut", "weapons/tfa_cso/qbb95/clipout.wav" )
TFA.AddWeaponSound( "Qbb95.ClipOn", "weapons/tfa_cso/qbb95/clipon.wav" )

--VSK94
TFA.AddFireSound( "VSK.Fire", "weapons/tfa_cso/vsk94/fire.wav", false, "^" )
TFA.AddWeaponSound( "VSK.Draw", "weapons/tfa_cso/vsk94/draw.wav" )
TFA.AddWeaponSound( "VSK.ClipIn", "weapons/tfa_cso/vsk94/clipin.wav" )
TFA.AddWeaponSound( "VSK.ClipOut", "weapons/tfa_cso/vsk94/clipout.wav" )

--Blood Dripper
TFA.AddFireSound( "Guilotine.Shoot", "weapons/tfa_cso/guilotine/shoot.wav", false, "^" )
TFA.AddFireSound( "Guilotine.Catch", "weapons/tfa_cso/guilotine/catch.wav", false, "^" )
TFA.AddWeaponSound( "Guilotine.Draw", "weapons/tfa_cso/guilotine/draw.wav" )
TFA.AddWeaponSound( "Guilotine.Draw_Empty", "weapons/tfa_cso/guilotine/draw_empty.wav" )
TFA.AddWeaponSound( "Guilotine.Exp", "weapons/tfa_cso/guilotine/exp.wav" )
TFA.AddWeaponSound( "Guilotine.Red", "weapons/tfa_cso/guilotine/red.wav" )

--Drakar-I/ Drakar-II
TFA.AddFireSound( "Drakar1.Fire", "weapons/tfa_cso/drakar1/fire.wav", false, "^" )
TFA.AddWeaponSound( "Drakar1.Draw", "weapons/tfa_cso/drakar1/draw.wav" )
TFA.AddWeaponSound( "Drakar1.ClipIn", "weapons/tfa_cso/drakar1/clipin.wav" )
TFA.AddWeaponSound( "Drakar1.ClipOut", "weapons/tfa_cso/drakar1/clipout.wav" )
TFA.AddWeaponSound( "Drakar1.Boltpull", "weapons/tfa_cso/drakar1/boltpull.wav" )
TFA.AddWeaponSound( "Drakar1.Boltpull01", "weapons/tfa_cso/drakar1/boltpull01.wav" )
TFA.AddWeaponSound( "Drakar1.Boltpull10", "weapons/tfa_cso/drakar1/boltpull10.wav" )
TFA.AddWeaponSound( "Drakar1.Beep", "weapons/tfa_cso/drakar1/Beep.wav" )
TFA.AddWeaponSound( "Drakar1.Slash1", "weapons/tfa_cso/drakar1/slash1.wav" )
TFA.AddWeaponSound( "Drakar1.Slash2", "weapons/tfa_cso/drakar1/slash2.wav" )
TFA.AddWeaponSound( "Drakar1.Slash3", "weapons/tfa_cso/drakar1/slash3.wav" )
TFA.AddWeaponSound( "Drakar1.Slash4", "weapons/tfa_cso/drakar1/slash4.wav" )
TFA.AddWeaponSound( "Drakar1.Slash_End", "weapons/tfa_cso/drakar1/slash_end.wav" )

--Drakar-III
TFA.AddFireSound( "Drakar3.Fire", "weapons/tfa_cso/drakar3/fire.wav", false, "^" )
TFA.AddWeaponSound( "Drakar3.Boltpull01", "weapons/tfa_cso/drakar3/boltpull01.wav" )
TFA.AddWeaponSound( "Drakar3.Boltpull10", "weapons/tfa_cso/drakar3/boltpull10.wav" )
TFA.AddWeaponSound( "Drakar3.Boltpull12", "weapons/tfa_cso/drakar3/boltpull12.wav" )
TFA.AddWeaponSound( "Drakar3.ClipIn20", "weapons/tfa_cso/drakar3/clipin20.wav" )
TFA.AddWeaponSound( "Drakar3.Beep", "weapons/tfa_cso/drakar3/Beep.wav" )
TFA.AddWeaponSound( "Drakar3.Slash1", "weapons/tfa_cso/drakar3/slash1.wav" )
TFA.AddWeaponSound( "Drakar3.Slash2", "weapons/tfa_cso/drakar3/slash2.wav" )
TFA.AddWeaponSound( "Drakar3.Slash3", "weapons/tfa_cso/drakar3/slash3.wav" )
TFA.AddWeaponSound( "Drakar3.Slash4", "weapons/tfa_cso/drakar3/slash4.wav" )
TFA.AddWeaponSound( "Drakar3.Slash_End", "weapons/tfa_cso/drakar3/slash_end.wav" )

--Bunker Buster
TFA.AddFireSound( "BunkerBuster.Gauge", "weapons/tfa_cso/bunker_buster/gauge.wav", false, "^" )
TFA.AddWeaponSound( "BunkerBuster.In", "weapons/tfa_cso/bunker_buster/zoom_in.wav" )
TFA.AddWeaponSound( "BunkerBuster.ZoomOut", "weapons/tfa_cso/bunker_buster/zoom_out.wav" )
TFA.AddWeaponSound( "BunkerBuster.Draw", "weapons/tfa_cso/bunker_buster/draw.wav" )
TFA.AddWeaponSound( "BunkerBuster.Clip", "weapons/tfa_cso/bunker_buster/clip.wav" )
TFA.AddWeaponSound( "BunkerBuster.Target", "weapons/tfa_cso/bunker_buster/target_siren.wav" )
TFA.AddWeaponSound( "BunkerBuster.Whistling1", "weapons/tfa_cso/bunker_buster/whistling1.wav" )
TFA.AddWeaponSound( "BunkerBuster.Whistling2", "weapons/tfa_cso/bunker_buster/whistling2.wav" )
TFA.AddWeaponSound( "BunkerBuster.Whistling3", "weapons/tfa_cso/bunker_buster/whistling3.wav" )
TFA.AddWeaponSound( "BunkerBuster.Fly", "weapons/tfa_cso/bunker_buster/flip.wav" )
TFA.AddWeaponSound( "BunkerBuster.Exp1", "weapons/tfa_cso/bunker_buster/exp1.wav" )
TFA.AddWeaponSound( "BunkerBuster.Exp2", "weapons/tfa_cso/bunker_buster/exp2.wav" )
TFA.AddWeaponSound( "BunkerBuster.Fire", "weapons/tfa_cso/bunker_buster/fire.wav" )
TFA.AddWeaponSound( "BunkerBuster.Clip", "weapons/tfa_cso/bunker_buster/clip.wav" )

--Claymore Mine MDS
TFA.AddWeaponSound( "Claymore.Draw_Off", "weapons/tfa_cso/claymore/draw_off.wav" )
TFA.AddWeaponSound( "Claymore.Draw", "weapons/tfa_cso/claymore/draw.wav" )
TFA.AddWeaponSound( "Claymore.Trigger_Draw", "weapons/tfa_cso/claymore/trigger_draw.wav" )
TFA.AddWeaponSound( "Claymore.Trigger_Off", "weapons/tfa_cso/claymore/trigger_off.wav" )
TFA.AddWeaponSound( "Claymore.Trigger_On", "weapons/tfa_cso/claymore/trigger_on.wav" )
TFA.AddWeaponSound( "Claymore.Trigger_Shoot_On", "weapons/tfa_cso/claymore/trigger_shoot_on.wav" )
TFA.AddWeaponSound( "Claymore.Exp", "weapons/tfa_cso/claymore/exp.wav" )
TFA.AddWeaponSound( "Claymore.Shoot", "weapons/tfa_cso/claymore/shoot.wav" )

--M249 Ra
TFA.AddFireSound( "M249Ra.Fire", "weapons/tfa_cso/m249ra/fire.wav", false, "^" )
TFA.AddFireSound( "M249Ra.Fire2", "weapons/tfa_cso/m249ra/fire2.wav", false, "^" )
TFA.AddWeaponSound( "M249Ra.Draw", "weapons/tfa_cso/m249ra/draw.wav" )
TFA.AddWeaponSound( "M249Ra.ClipIn3", "weapons/tfa_cso/m249ra/clipin3.wav" )
TFA.AddWeaponSound( "M249Ra.Idle", "weapons/tfa_cso/m249ra/idle.wav" )
TFA.AddWeaponSound( "M249Ra.Exp", "weapons/tfa_cso/m249ra/exp.wav" )

--Photon Launcher
TFA.AddFireSound( "PhotonLauncher.Fire", "weapons/tfa_cso/linkattacksg/fire.wav", false, "^" )
TFA.AddFireSound( "PhotonLauncher.Fire2", "weapons/tfa_cso/linkattacksg/fire2.wav", false, "^" )
TFA.AddFireSound( "PhotonLauncher.Fire3", "weapons/tfa_cso/linkattacksg/fire3.wav", false, "^" )
TFA.AddWeaponSound( "PhotonLauncher.Draw", "weapons/tfa_cso/linkattacksg/draw.wav" )
TFA.AddWeaponSound( "PhotonLauncher.Reload", "weapons/tfa_cso/linkattacksg/reload.wav" )
TFA.AddWeaponSound( "PhotonLauncher.Insert", "weapons/tfa_cso/linkattacksg/insert.wav" )
TFA.AddWeaponSound( "PhotonLauncher.Idle1", "weapons/tfa_cso/linkattacksg/idle1.wav" )
TFA.AddWeaponSound( "PhotonLauncher.Idle2", "weapons/tfa_cso/linkattacksg/idle2.wav" )
TFA.AddWeaponSound( "PhotonLauncher.Idle3", "weapons/tfa_cso/linkattacksg/idle3.wav" )

--Hyper Gaebolg
TFA.AddFireSound( "SpeargunEX.Fire", "weapons/tfa_cso/speargunex/fire.wav", false, "^" )
TFA.AddFireSound( "SpeargunEX.Fire2", "weapons/tfa_cso/speargunex/fire2.wav", false, "^" )
TFA.AddWeaponSound( "SpeargunEX.Exp", "weapons/tfa_cso/speargunex/exp.wav" )
TFA.AddWeaponSound( "SpeargunEX.Exp2", "weapons/tfa_cso/speargunex/exp2.wav" )
TFA.AddWeaponSound( "SpeargunEX.Draw", "weapons/tfa_cso/speargunex/draw.wav" )
TFA.AddWeaponSound( "SpeargunEX.Draw_Empty", "weapons/tfa_cso/speargunex/draw_empty.wav" )
TFA.AddWeaponSound( "SpeargunEX.Reload", "weapons/tfa_cso/speargunex/reload.wav" )
TFA.AddWeaponSound( "SpeargunEX.Charge_Start", "weapons/tfa_cso/speargunex/charge_start.wav" )
TFA.AddWeaponSound( "SpeargunEX.Charge_Shoot", "weapons/tfa_cso/speargunex/charge_shoot.wav" )
TFA.AddWeaponSound( "SpeargunEX.Spin11", "weapons/tfa_cso/speargunex/spin11.wav" )
TFA.AddWeaponSound( "SpeargunEX.Spin12", "weapons/tfa_cso/speargunex/spin12.wav" )
TFA.AddWeaponSound( "SpeargunEX.Spin21", "weapons/tfa_cso/speargunex/spin21.wav" )
TFA.AddWeaponSound( "SpeargunEX.Spin22", "weapons/tfa_cso/speargunex/spin22.wav" )