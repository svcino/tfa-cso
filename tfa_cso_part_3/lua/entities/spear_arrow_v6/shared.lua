
-- Copyright (c) 2018 TFA Base Devs

-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:

-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

ENT.Type = "anim"
ENT.Base = "base_gmodentity"
ENT.PrintName = "TFBow Arrow"
ENT.Author = "TheForgottenArchitect"
ENT.Contact = "Don't"
ENT.Purpose = "Arrow Entity"
ENT.Instructions = "Spawn this with a velocity, get rich"
ENT.Impacted = false
ENT.HitEntity = false
ENT.EntityOffset = false
ENT.BaseAngle = Angle(0, 0, 0)
ENT.ImpactVector = Vector(0, 0, 0)
ENT.TargetEntity = nil
ENT.LifeTime = 1
ENT.BlastDamage = 250
ENT.BlastRadius = 128
ENT.ImpactDamage = 65

local cv_al = GetConVar("sv_tfa_arrow_lifetime")
local cv_ht = GetConVar("host_timescale")
function ENT:Initialize()
		if(CLIENT) then return end
        self:PhysicsInit(SOLID_VPHYSICS)
        self:SetMoveType(MOVETYPE_VPHYSICS)
        self:SetSolid(SOLID_VPHYSICS)
	    util.SpriteTrail(self, 0, Color(153,204,255), false, 1, 0.5, 0.4, 0.125, "trails/smoke.vmt")
        local phys = self:GetPhysicsObject()
 		self.BaseAngle = self:GetAngles()
        if (phys:IsValid()) then
			phys:Wake()
			phys:SetMass(1)
			phys:EnableDrag(false)
			phys:EnableGravity(false)
			phys:SetBuoyancyRatio(0)

            if self.velocity then
                phys:SetVelocityInstantaneous(self.velocity)
            end
            self:StartMotionController()
        --    self:PhysicsUpdate(phys, 0.1 * cv_ht:GetFloat() )
        end
        self:NextThink(CurTime())
end

function ENT:Think()
	if(CLIENT) then return end
	if(self.Owner) then
		if(self.Owner:KeyDown(2048)) then
			self:Explosion()
		end
	end
	if(self.Impacted) then
		if(self.HitEntity) then
			if(IsValid(self.TargetEntity)) then
				self:SetPos(self.TargetEntity:GetPos() + self.EntityOffset)
			end
		else
			self:SetPos(self.ImpactVector)
		end
	end
	self:NextThink(CurTime())
	return true
end

function ENT:PhysicsCollide(data, phys)
	if(self.Impacted) then return end
	local ent = data.HitEntity
	local pos = data.HitPos
	self.HitEntity = IsValid(ent)
	if(IsValid(ent)) then
		ent:SetVelocity(self.BaseAngle:Forward() * 2048)
		local w = self.Owner:GetActiveWeapon()
		if(!IsValid(w)) then w = self end
		local d = CSO:DMGInfo(self.Owner, w, self.ImpactDamage, Vector(0, 0, 0), DMG_CLUB, self:GetPos())
		ent:TakeDamageInfo(d)
		self.TargetEntity = ent
		self.EntityOffset =  pos - ent:GetPos() -- Offset to impact position
	end
	self.ImpactVector = pos
	self:SetAngles(self.BaseAngle)
	timer.Simple(self.LifeTime, function()
		if(!IsValid(self) || !IsValid(self.Owner)) then return end
		self:Explosion()
	end)
	self:GetPhysicsObject():EnableMotion(false)
	self:GetPhysicsObject():EnableCollisions(false)
	self:SetCollisionGroup(1)
	self.Impacted = true
end

function ENT:Explosion()
	local w = self.Owner:GetActiveWeapon()
	if(!IsValid(w)) then w = self end
	for k,v in pairs(ents.FindInSphere(self:GetPos(), self.BlastRadius)) do
		if(!IsValid(v)) then continue end
		local dst = (v:GetPos():Distance(self:GetPos()))
		local dmg = self.BlastDamage * (1.1 - (dst / self.BlastRadius))
		local dmginfo = CSO:DMGInfo(self.Owner, w, dmg, Vector(0, 0, 0), DMG_BLAST, v:GetPos())
		if(v == self.Owner) then
			dmginfo:SetDamage(15)
			self.Owner:SetVelocity(Vector(0, 0, self.Owner:GetVelocity().z * -1) + Vector(0, 0, 325))
		end
		if(v == self.TargetEntity) then
			dmginfo:SetDamage(self.BlastDamage * 3)
		end
		v:TakeDamageInfo(dmginfo)
	end
	local e = EffectData()
	e:SetOrigin(self:GetPos())
	sound.Play("weapons/tfa_cso/fragnade/exp" ..math.random(1, 3) ..".wav", self:GetPos(), 120, 100, 1)
	util.Effect("HelicopterMegaBomb", e)
	self:Remove()
end