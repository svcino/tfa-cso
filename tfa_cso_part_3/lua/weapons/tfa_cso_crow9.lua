SWEP.Base = "tfa_melee_base"
SWEP.Category = "TFA CS:O Melees"
SWEP.PrintName = "CROW-9"
SWEP.Author	= "★Bullet★, Meika" --Author Tooltip
SWEP.Type	= "Rare Grade Melee"
SWEP.ViewModel = "models/weapons/tfa_cso/c_crow9.mdl"
SWEP.WorldModel = "models/weapons/tfa_cso/w_crow9_r.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 80
SWEP.UseHands = true
SWEP.HoldType = "fist"
SWEP.DrawCrosshair = true
SWEP.Secondary.Automatic = true
SWEP.Primary.Directional = false

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.DisableIdleAnimations = false
--SWEP.StabMissTable = {"ACT_VM_PULLBACK"} --Table of possible hull sequences

SWEP.Secondary.CanBash = false

--[[INSPECTION]]--

SWEP.InspectPos = Vector (0,0,0) --Replace with a vector, in style of ironsights position, to be used for inspection
SWEP.InspectAng = Vector (0,0,0) --Replace with a vector, in style of ironsights angle, to be used for inspection
SWEP.InspectionLoop = true --Setting false will cancel inspection once the animation is done.  CS:GO style.

-- nZombies Stuff
SWEP.NZWonderWeapon		= false	-- Is this a Wonder-Weapon? If true, only one player can have it at a time. Cheats aren't stopped, though.
--SWEP.NZRePaPText		= "your text here"	-- When RePaPing, what should be shown? Example: Press E to your text here for 2000 points.
SWEP.NZPaPName				= "Mortal CROWbat 18"
--SWEP.NZPaPReplacement 	= "tfa_cso_dualsword"	-- If Pack-a-Punched, replace this gun with the entity class shown here.
SWEP.NZPreventBox		= false	-- If true, this gun won't be placed in random boxes GENERATED. Users can still place it in manually.
SWEP.NZTotalBlackList	= false	-- if true, this gun can't be placed in the box, even manually, and can't be bought off a wall, even if placed manually. Only code can give this gun.
SWEP.PaPMats			= {}

SWEP.WElements = {
	["crow9_l"] = { type = "Model", model = "models/weapons/tfa_cso/w_crow9_l.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(4, 2.5, 0), angle = Angle(170, 170, 120), size = Vector(1.2, 1.2, 1.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Precision = 50
SWEP.Secondary.MaxCombo = -1
SWEP.Primary.MaxCombo = -1

SWEP.Offset = {
		Pos = {
		Up = -0.5,
		Right = 2.5,
		Forward = 4.5,
		},
		Ang = {
		Up = -190,
		Right = 190,
		Forward = 75
		},
		Scale = 1.2
}

sound.Add({
	['name'] = "CROW9.Draw",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/crow9/draw.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "CROW9.Hit",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/crow9/hit1.wav", "weapons/tfa_cso/crow9/hit2.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "CROW9.HitWall",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/crow9/hitwall.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "CROW9.SlashA",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/crow9/slasha_1.wav"},
	['pitch'] = {100,100}
})

SWEP.Primary.Attacks = {
	{
		['act'] = ACT_VM_HITRIGHT, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 15*5, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(40,0,0), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 40, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_CLUB, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.10, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "CROW9.SlashA", -- Sound ID
		['snd_delay'] = 0.01,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 0.35, --time before next attack
		['hull'] = 32, --Hullsize
		['direction'] = "F", --Swing dir,
		['hitflesh'] = "CROW9.Hit",
		['hitworld'] = "CROW9.HitWall",
		['maxhits'] = 25
	},
	{
		['act'] = ACT_VM_HITLEFT, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 15*5, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(-20,0,-30), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 40, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_CLUB, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.10, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "CROW9.SlashA", -- Sound ID
		['snd_delay'] = 0.01,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 0.35, --time before next attack
		['hull'] = 32, --Hullsize
		['direction'] = "F", --Swing dir,
		['hitflesh'] = "CROW9.Hit",
		['hitworld'] = "CROW9.HitWall",
		['maxhits'] = 25
	}
}

DEFINE_BASECLASS(SWEP.Base)

SWEP.NextAbilityTime = 3.5
SWEP.CurAbility = -1

SWEP.FailTimer1 = -1
SWEP.FailTimer2 = -1

SWEP.IsInAbilityMode = false

SWEP.BashKnockback = 2500

--[[
	4 - Start
	5 - Success
	6 - Failed
]]

SWEP.ReloadKeyDown = false
function SWEP:SecondaryAttack()
	if(CSO:BlockExtraAction(self) || self:GetNextPrimaryFire() > CurTime()) then return end
	self.Owner:EmitSound("weapons/tfa_cso/crow9/slashc_in.wav")
	self:GetOwner():SetAnimation(PLAYER_ATTACK1)
	CSO:QuickMeleeTrace(self, 8, Vector(8, 8, 2), 32, 80, 15*4, "CROW9.Hit", "CROW9.HitWall")
	CSO:ForceVMSequenceEvent(self, 4, nil, false, true, function()
		self.IsInAbilityMode = false
		if(CSO:GetVMSequence(self.Owner) != 4) then return end
		self.Owner:EmitSound("weapons/tfa_cso/crow9/slashc_2.wav")
		CSO:ForceVMSequence(self, 6, nil, true)
	end)
	self.FailTimer1 = CurTime() + 0.845
	self.FailTimer2 = CurTime() + 0.955
	self:SetNextIdleAnim(CurTime() + self.NextAbilityTime)
	CSO:SetNextActionTime(self, self.NextAbilityTime)
	self.IsInAbilityMode = true
end

SWEP.AbilityDamage = 120
SWEP.AbilityRange = 100
function SWEP:Think2(...)
	local seq = CSO:GetVMSequence(self.Owner)
	local ply = self.Owner
	local reloadkey = ply:KeyDown(2048)

	if(!self.ReloadKeyDown && reloadkey) then
		if(seq == 4 && self.IsInAbilityMode && self.FailTimer1 < CurTime() && self.FailTimer2 > CurTime()) then
			if(SERVER) then
				self:GetOwner():SetAnimation(PLAYER_ATTACK1)
				CSO:QuickMeleeTrace(self, 8, Vector(8, 8, 2), self.BashKnockback, self.AbilityDamage, self.AbilityRange, "CROW9.HitWall", "CROW9.Hit")
			end
			self:EmitSound("weapons/tfa_cso/crow9/slashc_1.wav")
			CSO:ForceVMSequence(self, 5, nil, true)
		end
	end

	self.ReloadKeyDown = reloadkey

	BaseClass.Think2(self, ...)
end