# TFA: Counter-Strike: Online / Counter-Strike Nexon: Zombies SWEPs

Weapon ports of Counter-Strike Online's weapons for TFA Base on Garry's Mod. Some are missing features and have noticable bugs.
Merge requests welcome! Please share your improvements. Let me (Global) know when you submit one, preferably on Steam. Thanks!

## Workshop Download

* Part 1: https://steamcommunity.com/workshop/filedetails/?id=712848264
* Part 2: https://steamcommunity.com/sharedfiles/filedetails/?id=1309914309
* Part 3: https://steamcommunity.com/sharedfiles/filedetails/?id=1538229351
* Part 4: https://steamcommunity.com/sharedfiles/filedetails/?id=1845577793
* Required: https://steamcommunity.com/workshop/filedetails/?id=2840031720

The Git version of TFA CS:O is cutting edge. This means you might get more weapons then the workshop version, at the cost of more bugs and incomplete stuff. Unless told otherwise, you should really use the workshop version!