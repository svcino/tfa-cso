AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
include('shared.lua')

function ENT:Initialize()
	
	self.Entity:SetModel("models/weapons/tfa_cso/w_zongzi.mdl")
	
	self:PhysicsInit(SOLID_VPHYSICS)
	--self.Entity:PhysicsInitSphere( ( self:OBBMaxs() - self:OBBMins() ):Length()/4, "metal" )
	self.Entity:SetMoveType( MOVETYPE_VPHYSICS )
	self.Entity:SetSolid( SOLID_VPHYSICS )
	self:SetModelScale(1)
	self.Entity:DrawShadow( false )
	self.Entity:SetCollisionGroup( COLLISION_GROUP_WEAPON )
	self:SetAngles(Angle(0, 90, 0) + self.Owner:EyeAngles())
	
	local phys = self.Entity:GetPhysicsObject()
	if (phys:IsValid()) then
		phys:Wake()
		phys:SetMass(6.5)
		phys:SetDamping(0.1,5)
	end
	
	self:SetFriction(3)
	
	self.timeleft = CurTime() + 1 -- HOW LONG BEFORE EXPLOSION
	self:Think()
	self.NextExplode = CurTime() + self.MidDelay
end

ENT.Delay = 3
ENT.MidDelay = 0.35
ENT.MaxExplodes = 0

function ENT:Think()
if self.timeleft < CurTime() then
  if CurTime()>self.NextExplode then
	self:Explosion()
	self.NextExplode = CurTime() + self.MidDelay
	self.exp = ( self.exp or 0 ) + 1
	if self.exp > self.MaxExplodes then
	  self:Remove()
	end
  end
end
end

ENT.InstaKillChance = 20
ENT.ExplosionRadius = 256

function ENT:Explosion()

	if not IsValid(self.Owner) then
		self.Entity:Remove()
		return
	end

	local effectdata = EffectData()
		effectdata:SetOrigin(self.Entity:GetPos())
	util.Effect("exp_firecracker", effectdata)

	for k,v in pairs(ents.FindInSphere(self:GetPos(), self.ExplosionRadius)) do
		if(!IsValid(v)) then continue end
		if(!v:IsPlayer() && !v:IsNPC()) then continue end
		local scale = 1.1 - (CSO:GetDistance(self, v) / self.ExplosionRadius)
		local rand = math.random(1, 100)
		local dmg = 400 * scale
		if(rand < self.InstaKillChance) then
			dmg = v:Health() + 1
		end
		v:TakeDamageInfo(CSO:DMGInfo(self.Owner, self.Entity, dmg, Vector(0, 0, 0), 64, v:GetPos()))
	end
	
	local shake = ents.Create("env_shake")
		shake:SetOwner(self.Owner)
		shake:SetPos(self.Entity:GetPos())
		shake:SetKeyValue("amplitude", "2200")	// Power of the shake
		shake:SetKeyValue("radius", "1400")		// Radius of the shake
		shake:SetKeyValue("duration", "3")	// Time of shake
		shake:SetKeyValue("frequency", "255")	// How har should the screenshake be
		shake:SetKeyValue("spawnflags", "4")	// Spawnflags(In Air)
		shake:Spawn()
		shake:Activate()
		
		self.Entity:EmitSound("weapons/tfa_cso/mooncake/boom.wav", 100, 100 )

end

/*---------------------------------------------------------
OnTakeDamage
---------------------------------------------------------*/
function ENT:OnTakeDamage( dmginfo )
end


/*---------------------------------------------------------
Use
---------------------------------------------------------*/
function ENT:Use( activator, caller, type, value )
end


/*---------------------------------------------------------
StartTouch
---------------------------------------------------------*/
function ENT:StartTouch( entity )
end


/*---------------------------------------------------------
EndTouch
---------------------------------------------------------*/
function ENT:EndTouch( entity )
end


/*---------------------------------------------------------
Touch
---------------------------------------------------------*/
function ENT:Touch( entity )
end