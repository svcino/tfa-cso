--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--BALROG-3
TFA.AddFireSound( "BALROG3.Fire", "weapons/tfa_cso/balrog3/fire.wav", false, "^" )
TFA.AddWeaponSound( "BALROG3.ClipOut", "weapons/tfa_cso/balrog3/clipout.wav")
TFA.AddWeaponSound( "BALROG3.ClipIn", "weapons/tfa_cso/balrog3/clipin.wav")
TFA.AddWeaponSound( "BALROG3.BoltPull", "weapons/tfa_cso/balrog3/boltpull.wav")
TFA.AddWeaponSound( "BALROG3.Draw", "weapons/tfa_cso/balrog3/draw.wav")

--Dual Kriss Custom
TFA.AddFireSound( "DualKrissCustom.Fire", "weapons/tfa_cso/dualkrisscustom/fire.wav", false, "^" )
TFA.AddWeaponSound( "DualKrissCustom.ClipOut", "weapons/tfa_cso/dualkrisscustom/clipout.wav")
TFA.AddWeaponSound( "DualKrissCustom.ClipIn", "weapons/tfa_cso/dualkrisscustom/clipin.wav")
TFA.AddWeaponSound( "DualKrissCustom.Draw", "weapons/tfa_cso/dualkrisscustom/draw.wav")

--Dual Uzi
TFA.AddFireSound( "DualUzi.Fire", "weapons/tfa_cso/dualuzi/fire.wav", false, "^" )
TFA.AddWeaponSound( "DualUzi.ClipOut", "weapons/tfa_cso/dualuzi/clipout.wav")
TFA.AddWeaponSound( "DualUzi.ClipIn", "weapons/tfa_cso/dualuzi/clipin.wav")
TFA.AddWeaponSound( "DualUzi.Draw", "weapons/tfa_cso/dualuzi/draw.wav")
TFA.AddWeaponSound( "DualUzi.Idle1", "weapons/tfa_cso/dualuzi/idle1.wav")
TFA.AddWeaponSound( "DualUzi.Idle2", "weapons/tfa_cso/dualuzi/idle2.wav")

--Laser Storm
TFA.AddFireSound( "LaserStorm.Fire", "weapons/tfa_cso/laser_storm/fire.wav", false, "^" )
TFA.AddFireSound( "LaserStorm.Fire2_Loop", "weapons/tfa_cso/laser_storm/fire2_loop.wav", false, "^" )
TFA.AddFireSound( "LaserStorm.Fire2_End", "weapons/tfa_cso/laser_storm/fire2_end.wav", false, "^" )
TFA.AddWeaponSound( "LaserStorm.ClipOut", "weapons/tfa_cso/laser_storm/clipout.wav")
TFA.AddWeaponSound( "LaserStorm.ClipIn1", "weapons/tfa_cso/laser_storm/clipin1.wav")
TFA.AddWeaponSound( "LaserStorm.ClipIn2", "weapons/tfa_cso/laser_storm/clipin2.wav")
TFA.AddWeaponSound( "LaserStorm.Draw", "weapons/tfa_cso/laser_storm/draw.wav")
TFA.AddWeaponSound( "LaserStorm.Shootb_End", "weapons/tfa_cso/laser_storm/shootb_end.wav")
