--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--Anaconda
TFA.AddFireSound( "Anaconda.Fire", "weapons/tfa_cso/anaconda/fire.wav", false, "^" )
TFA.AddWeaponSound( "Anaconda.Foley1", "weapons/tfa_cso/anaconda/foley_1.wav")
TFA.AddWeaponSound( "Anaconda.Foley2", "weapons/tfa_cso/anaconda/foley_2.wav")
TFA.AddWeaponSound( "Anaconda.Foley3", "weapons/tfa_cso/anaconda/foley_3.wav")
TFA.AddWeaponSound( "Anaconda.Foley4", "weapons/tfa_cso/anaconda/foley_4.wav")
TFA.AddWeaponSound( "Anaconda.Foley5", "weapons/tfa_cso/anaconda/foley_5.wav")

--BALROG-1
TFA.AddFireSound( "BALROG1.Fire", "weapons/tfa_cso/balrog1/fire.wav", false, "^" )
TFA.AddFireSound( "BALROG1.Fire2", "weapons/tfa_cso/balrog1/fire2.wav", false, "^" )
TFA.AddWeaponSound( "BALROG1.Draw", "weapons/tfa_cso/balrog1/draw.wav" )
TFA.AddWeaponSound( "BALROG1.ChangeA", "weapons/tfa_cso/balrog1/changea.wav" )
TFA.AddWeaponSound( "BALROG1.ChangeB", "weapons/tfa_cso/balrog1/changeb.wav" )
TFA.AddWeaponSound( "BALROG1.Reload", "weapons/tfa_cso/balrog1/reload.wav" )
TFA.AddWeaponSound( "BALROG1.ReloadB", "weapons/tfa_cso/balrog1/reloadb.wav" )

--Crimson Hunter
TFA.AddFireSound( "CrimsonHunter.Fire", "weapons/tfa_cso/crimson_hunter/fire.wav", false, "^" )
TFA.AddFireSound( "CrimsonHunter.Throw", "weapons/tfa_cso/crimson_hunter/throw.wav", false, "^" )
TFA.AddWeaponSound( "CrimsonHunter.ClipOut", "weapons/tfa_cso/crimson_hunter/clipout.wav" )
TFA.AddWeaponSound( "CrimsonHunter.ClipIn", "weapons/tfa_cso/crimson_hunter/clipin.wav" )
TFA.AddWeaponSound( "CrimsonHunter.Idle", "weapons/tfa_cso/crimson_hunter/idle.wav" )
TFA.AddWeaponSound( "CrimsonHunter.Change", "weapons/tfa_cso/crimson_hunter/change.wav" )

--CROW-1
TFA.AddFireSound( "CROW1.Fire", "weapons/tfa_cso/crow1/fire.wav", false, "^" )
TFA.AddWeaponSound( "CROW1.Draw", "weapons/tfa_cso/crow1/draw.wav" )
TFA.AddWeaponSound( "CROW1.ClipOut", "weapons/tfa_cso/crow1/clipout.wav" )
TFA.AddWeaponSound( "CROW1.ClipIn1", "weapons/tfa_cso/crow1/clipin_1.wav" )
TFA.AddWeaponSound( "CROW1.ClipIn2", "weapons/tfa_cso/crow1/clipin_2.wav" )
TFA.AddWeaponSound( "CROW1.ReloadB1", "weapons/tfa_cso/crow1/reloadb_1.wav" )
TFA.AddWeaponSound( "CROW1.ReloadB2", "weapons/tfa_cso/crow1/reloadb_2.wav" )


--Desperado
TFA.AddFireSound( "Desperado.Fire", "weapons/tfa_cso/desperado/fire.wav", false, "^" )
TFA.AddWeaponSound( "Desperado.Reload", "weapons/tfa_cso/desperado/reload.wav" )

--Divine Lock
TFA.AddFireSound( "Flintlock.Fire", "weapons/flintlock/fire.wav", false, "^" )
TFA.AddWeaponSound( "Flintlock.Draw", "weapons/flintlock/draw.wav" )
TFA.AddWeaponSound( "Flintlock.In 1", "weapons/flintlock/in1.wav" )
TFA.AddWeaponSound( "Flintlock.In 2", "weapons/flintlock/in2.wav" )
TFA.AddWeaponSound( "Flintlock.In 3", "weapons/flintlock/in3.wav" )

--Special Duck Foot Gun
TFA.AddFireSound( "DuckGun.Fire", "weapons/tfa_cso/duck_gun/fire.wav", false, "^" )
TFA.AddWeaponSound( "DuckGun.BoltPull", "weapons/tfa_cso/duck_gun/boltpull.wav")
TFA.AddWeaponSound( "DuckGun.ClipOut1", "weapons/tfa_cso/duck_gun/clipout1.wav")
TFA.AddWeaponSound( "DuckGun.ClipOut2", "weapons/tfa_cso/duck_gun/clipout2.wav")
TFA.AddWeaponSound( "DuckGun.ClipIn1", "weapons/tfa_cso/duck_gun/clipin1.wav")
TFA.AddWeaponSound( "DuckGun.ClipIn2", "weapons/tfa_cso/duck_gun/clipin2.wav")
TFA.AddWeaponSound( "DuckGun.Draw", "weapons/tfa_cso/duck_gun/clipin2.wav") --yes i know its the same sound shut up

--FNP-45
TFA.AddFireSound( "FNP45.Fire", "weapons/tfa_cso/fnp45/fire.wav", false, "^" )
TFA.AddWeaponSound( "FNP45.ClipOut", "weapons/tfa_cso/fnp45/clipout.wav")
TFA.AddWeaponSound( "FNP45.ClipIn1", "weapons/tfa_cso/fnp45/clipin_1.wav")
TFA.AddWeaponSound( "FNP45.ClipIn2", "weapons/tfa_cso/fnp45/clipin_2.wav")
TFA.AddWeaponSound( "FNP45.Draw", "weapons/tfa_cso/fnp45/draw.wav")

--Luger (and all variants)
TFA.AddFireSound( "Luger.Fire", "weapons/tfa_cso/luger/fire.wav", false, "^" )
TFA.AddWeaponSound( "Luger.ClipOut1", "weapons/tfa_cso/luger/clipout_1.wav")
TFA.AddWeaponSound( "Luger.ClipOut2", "weapons/tfa_cso/luger/clipout_2.wav")
TFA.AddWeaponSound( "Luger.ClipIn", "weapons/tfa_cso/luger/clipin.wav")
TFA.AddWeaponSound( "Luger.SlideBack", "weapons/tfa_cso/luger/slideback.wav")

--M1911A1
TFA.AddFireSound( "M1911A1.Fire", "weapons/tfa_cso/m1911a1/fire.wav", false, "^" )
TFA.AddWeaponSound( "M1911A1.ClipIn", "weapons/tfa_cso/m1911a1/clipin.wav")
TFA.AddWeaponSound( "M1911A1.SlideBack", "weapons/tfa_cso/m1911a1/slideback.wav")
TFA.AddWeaponSound( "M1911A1.ClipOut", "weapons/tfa_cso/m1911a1/clipout.wav")

--Mauser C96
TFA.AddFireSound( "MauserC96.Fire", "weapons/tfa_cso/mauserc96/fire.wav", false, "^" )
TFA.AddWeaponSound( "MauserC96.Draw", "weapons/tfa_cso/mauserc96/draw.wav")
TFA.AddWeaponSound( "MauserC96.ClipOut", "weapons/tfa_cso/mauserc96/clipout.wav")
TFA.AddWeaponSound( "MauserC96.ClipIn", "weapons/tfa_cso/mauserc96/clipin.wav")
TFA.AddWeaponSound( "MauserC96.BoltPull", "weapons/tfa_cso/mauserc96/boltpull.wav")
TFA.AddWeaponSound( "MauserC96.BoltPull_Empty", "weapons/tfa_cso/mauserc96/boltpull_empty.wav")


--SKULL-1
TFA.AddFireSound( "Skull1.Fire", "weapons/tfa_cso/skull1/fire.wav", false, "^" )
TFA.AddWeaponSound( "Skull1.ClipOut", "weapons/tfa_cso/skull1/clipout.wav")
TFA.AddWeaponSound( "Skull1.ClipIn", "weapons/tfa_cso/skull1/clipin.wav")
TFA.AddWeaponSound( "Skull1.Draw", "weapons/tfa_cso/skull1/draw.wav")

--SKULL-2
TFA.AddFireSound( "SKULL2.Fire", "weapons/tfa_cso/skull2/fire.wav", false, "^" )
TFA.AddWeaponSound( "SKULL2.ReloadLeft", "weapons/tfa_cso/skull2/reload_left.wav")
TFA.AddWeaponSound( "SKULL2.ReloadRight", "weapons/tfa_cso/skull2/reload_right.wav")
TFA.AddWeaponSound( "SKULL2.Draw", "weapons/tfa_cso/skull2/draw.wav")

--THANATOS-1
TFA.AddFireSound( "THANATOS1.Fire", "weapons/tfa_cso/thanatos1/fire.wav", false, "^" )
TFA.AddWeaponSound( "THANATOS1.ClipOut", "weapons/tfa_cso/thanatos1/clipout.wav")
TFA.AddWeaponSound( "THANATOS1.ClipIn", "weapons/tfa_cso/thanatos1/clipin.wav")
TFA.AddWeaponSound( "THANATOS1.BoltPull", "weapons/tfa_cso/thanatos1/boltpull.wav")

--TURBULENT-1
TFA.AddFireSound( "TURBULENT1.Fire", "weapons/tfa_cso/turbulent_1/fire.wav", false, "^" )
TFA.AddWeaponSound( "TURBULENT1.ClipOut", "weapons/tfa_cso/turbulent_1/clipout.wav")
TFA.AddWeaponSound( "TURBULENT1.ClipIn1", "weapons/tfa_cso/turbulent_1/clipin_1.wav")
TFA.AddWeaponSound( "TURBULENT1.ClipIn2", "weapons/tfa_cso/turbulent_1/clipin_2.wav")
TFA.AddWeaponSound( "TURBULENT1.ClipIn3", "weapons/tfa_cso/turbulent_1/clipin_3.wav")
TFA.AddWeaponSound( "TURBULENT1.Draw", "weapons/tfa_cso/turbulent_1/draw.wav")

--VULCANUS-1
TFA.AddFireSound( "VULCANUS1.Fire", "weapons/tfa_cso/vulcanus1/fire_a.wav", false, "^" )
TFA.AddFireSound( "VULCANUS1.FireB", "weapons/tfa_cso/vulcanus1/fire_b.wav", false, "^" )
TFA.AddWeaponSound( "VULCANUS1.Draw", "weapons/tfa_cso/vulcanus1/draw.wav")
TFA.AddWeaponSound( "VULCANUS1.ClipOut", "weapons/tfa_cso/vulcanus1/clipout.wav")
TFA.AddWeaponSound( "VULCANUS1.ClipInA", "weapons/tfa_cso/vulcanus1/clipin_a.wav")
TFA.AddWeaponSound( "VULCANUS1.Reload", "weapons/tfa_cso/vulcanus1/reload.wav")
TFA.AddWeaponSound( "VULCANUS1.DrawB", "weapons/tfa_cso/vulcanus1/draw_b.wav")
TFA.AddWeaponSound( "VULCANUS1.ClipInB", "weapons/tfa_cso/vulcanus1/clipin_b.wav")
TFA.AddWeaponSound( "VULCANUS1.ChangeToA", "weapons/tfa_cso/vulcanus1/change_to_a.wav")
TFA.AddWeaponSound( "VULCANUS1.ChangeToB", "weapons/tfa_cso/vulcanus1/change_to_b.wav")

--OZ LION PISTOL
TFA.AddFireSound( "OZWPNSET2.Fire", "weapons/tfa_cso/ozwpnset2/fire.wav", false, "^" )
TFA.AddWeaponSound( "OZWPNSET2.ClipOut", "weapons/tfa_cso/ozwpnset2/clipout.wav")
TFA.AddWeaponSound( "OZWPNSET2.ClipIn", "weapons/tfa_cso/ozwpnset2/clipin.wav")
TFA.AddWeaponSound( "OZWPNSET2.Idle1", "weapons/tfa_cso/ozwpnset2/idle1.wav")
TFA.AddWeaponSound( "OZWPNSET2.Idle2", "weapons/tfa_cso/ozwpnset2/idle2.wav")
TFA.AddWeaponSound( "OZWPNSET2.Idle3", "weapons/tfa_cso/ozwpnset2/idle3.wav")
TFA.AddWeaponSound( "OZWPNSET2.Draw", "weapons/tfa_cso/ozwpnset2/draw.wav")

--Wild Wing
TFA.AddFireSound( "Catapult.Fire", "weapons/tfa_cso/wild_wing/fire.wav", false, "^" )
TFA.AddFireSound( "Catapult.Fire2", "weapons/tfa_cso/wild_wing/fire2.wav", false, "^" )
TFA.AddWeaponSound( "Catapult.ShootIdle1", "weapons/tfa_cso/wild_wing/shootidle1.wav")
TFA.AddWeaponSound( "Catapult.Draw", "weapons/tfa_cso/wild_wing/draw.wav")

--Oz Tin Robot Machine Gun
TFA.AddFireSound( "OZWPNSET1.Fire", "weapons/tfa_cso/ozwpnset1/fire.wav", false, "^" )
TFA.AddFireSound( "OZWPNSET1.Fire_Change", "weapons/tfa_cso/ozwpnset1/fire_change.wav", false, "^" )
TFA.AddWeaponSound( "OZWPNSET1.Reload", "weapons/tfa_cso/ozwpnset1/reload.wav")
TFA.AddWeaponSound( "OZWPNSET1.Idle", "weapons/tfa_cso/ozwpnset1/idle.wav")
TFA.AddWeaponSound( "OZWPNSET1.Shoot_Empty", "weapons/tfa_cso/ozwpnset1/shoot_empty.wav")

--AUG
TFA.AddFireSound( "AUG.Fire", "weapons/tfa_cso/aug/fire.wav", false, "^" )
TFA.AddWeaponSound( "AUG.Draw", "weapons/tfa_cso/aug/draw.wav")
TFA.AddWeaponSound( "AUG.ClipOut", "weapons/tfa_cso/aug/clipout.wav")
TFA.AddWeaponSound( "AUG.ClipIn", "weapons/tfa_cso/aug/clipin.wav")
TFA.AddWeaponSound( "AUG.BoltPull", "weapons/tfa_cso/aug/boltpull.wav")
TFA.AddWeaponSound( "AUG.BoltSlap", "weapons/tfa_cso/aug/boltslap.wav")

--G3SG-1
TFA.AddFireSound( "G3SG1.Fire", "weapons/tfa_cso/g3sg1/fire.wav", false, "^" )
TFA.AddWeaponSound( "G3SG1.ClipOut", "weapons/tfa_cso/g3sg1/clipout.wav")
TFA.AddWeaponSound( "G3SG1.ClipIn", "weapons/tfa_cso/g3sg1/clipin.wav")
TFA.AddWeaponSound( "G3SG1.Slide", "weapons/tfa_cso/g3sg1/slide.wav")

--M3
TFA.AddFireSound( "M3.Fire", "weapons/tfa_cso/m3/fire.wav", false, "^" )
TFA.AddWeaponSound( "M3.Pump", "weapons/tfa_cso/m3/pump.wav")
TFA.AddWeaponSound( "M3.Insert", "weapons/tfa_cso/m3/insert.wav")

--SG552
TFA.AddFireSound( "SG552.Fire", "weapons/tfa_cso/sg552/fire.wav", false, "^" )
TFA.AddWeaponSound( "SG552.ClipOut", "weapons/tfa_cso/sg552/clipout.wav")
TFA.AddWeaponSound( "SG552.ClipIn", "weapons/tfa_cso/sg552/clipin.wav")
TFA.AddWeaponSound( "SG552.BoltPull", "weapons/tfa_cso/sg552/boltpull.wav")

--Water Balloon
TFA.AddWeaponSound( "WaterBalloon.Deploy", "weapons/tfa_cso/waterballoon/deploy.wav")
TFA.AddWeaponSound( "WaterBalloon.PullPin", "weapons/tfa_cso/waterballoon/pullpin.wav")

--Air Burster
TFA.AddFireSound( "AirBurster.Fire", "weapons/tfa_cso/airburster/fire.wav", false, "^" )
TFA.AddFireSound( "AirBurster.Fire2", "weapons/tfa_cso/airburster/fire2.wav", false, "^" )
TFA.AddFireSound( "AirBurster.Fire_End", "weapons/tfa_cso/airburster/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "AirBurster.Draw", "weapons/tfa_cso/airburster/draw.wav")
TFA.AddWeaponSound( "AirBurster.Idle", "weapons/tfa_cso/airburster/idle.wav")
TFA.AddWeaponSound( "AirBurster.ClipOut", "weapons/tfa_cso/airburster/clipout.wav")
TFA.AddWeaponSound( "AirBurster.ClipIn1", "weapons/tfa_cso/airburster/clipin1.wav")
TFA.AddWeaponSound( "AirBurster.ClipIn2", "weapons/tfa_cso/airburster/clipin2.wav")
TFA.AddWeaponSound( "AirBurster.ClipIn3", "weapons/tfa_cso/airburster/clipin3.wav")
TFA.AddWeaponSound( "AirBurster.ClipIn4", "weapons/tfa_cso/airburster/clipin4.wav")

--Leviathan
TFA.AddFireSound( "Leviathan.Fire", "weapons/tfa_cso/watercannon/fire.wav", false, "^" )
TFA.AddFireSound( "Leviathan.Fire_End", "weapons/tfa_cso/watercannon/fire_end.wav", false, "^" )
TFA.AddWeaponSound( "Leviathan.Draw", "weapons/tfa_cso/watercannon/draw.wav")
TFA.AddWeaponSound( "Leviathan.ClipOut", "weapons/tfa_cso/watercannon/clipout.wav")
TFA.AddWeaponSound( "Leviathan.ClipIn", "weapons/tfa_cso/watercannon/clipin.wav")

--AUG Guardians of the Galaxy
TFA.AddFireSound( "AUG_Gs.Fire", "weapons/tfa_cso/aug_guardians/fire.wav", false, "^" )
TFA.AddFireSound( "AUG_Gs.Skill1", "weapons/tfa_cso/aug_guardians/skill1.wav", false, "^" )
TFA.AddWeaponSound( "AUG_Gs.Draw", "weapons/tfa_cso/aug_guardians/draw.wav")
TFA.AddWeaponSound( "AUG_Gs.ClipOut", "weapons/tfa_cso/aug_guardians/clipout.wav")
TFA.AddWeaponSound( "AUG_Gs.ClipIn", "weapons/tfa_cso/aug_guardians/clipin.wav")
TFA.AddWeaponSound( "AUG_Gs.Idle1", "weapons/tfa_cso/aug_guardians/idle1.wav")
TFA.AddWeaponSound( "AUG_Gs.Idle2", "weapons/tfa_cso/aug_guardians/idle2.wav")
TFA.AddWeaponSound( "AUG_Gs.Change1", "weapons/tfa_cso/aug_guardians/change1.wav")
TFA.AddWeaponSound( "AUG_Gs.Change2", "weapons/tfa_cso/aug_guardians/change2.wav")

--IGNITE-7
TFA.AddFireSound( "IgniteMG.Fire", "weapons/tfa_cso/ignitemg/fire1.wav", false, "^" )
TFA.AddFireSound( "IgniteMG.Shoot_Loop", "weapons/tfa_cso/ignitemg/shoot2_loop.wav", false, "^" )
TFA.AddFireSound( "IgniteMG.Shoot_Start", "weapons/tfa_cso/ignitemg/shoot2_start.wav", false, "^" )
TFA.AddFireSound( "IgniteMG.Shoot_End", "weapons/tfa_cso/ignitemg/shoot2_end.wav", false, "^" )
TFA.AddWeaponSound( "IgniteMG.Draw1", "weapons/tfa_cso/ignitemg/draw1.wav")
TFA.AddWeaponSound( "IgniteMG.Draw2", "weapons/tfa_cso/ignitemg/draw2.wav")
TFA.AddWeaponSound( "IgniteMG.Reload", "weapons/tfa_cso/ignitemg/reload.wav")
TFA.AddWeaponSound( "IgniteMG.Shoot_Empty", "weapons/tfa_cso/ignitemg/shoot_empty.wav")
TFA.AddWeaponSound( "IgniteMG.Shoot_FX", "weapons/tfa_cso/ignitemg/shoot_fx.wav")

--M134 Minigun Umbra
TFA.AddFireSound( "ChainMG.Fire", "weapons/tfa_cso/chainmg/fire.wav", false, "^" )
TFA.AddFireSound( "ChainMG.Fire2", "weapons/tfa_cso/chainmg/fire2.wav", false, "^" )
TFA.AddWeaponSound( "ChainMG.Draw", "weapons/tfa_cso/chainmg/draw.wav")
TFA.AddWeaponSound( "ChainMG.ClipOff1", "weapons/tfa_cso/chainmg/clipoff1.wav")
TFA.AddWeaponSound( "ChainMG.ClipOff2", "weapons/tfa_cso/chainmg/clipoff2.wav")
TFA.AddWeaponSound( "ChainMG.ClipOn1", "weapons/tfa_cso/chainmg/clipon1.wav")
TFA.AddWeaponSound( "ChainMG.ClipOn2", "weapons/tfa_cso/chainmg/clipon2.wav")

--Naberius
TFA.AddFireSound( "Naberius.Fire", "weapons/tfa_cso/naberius/fire.wav", false, "^" )
TFA.AddFireSound( "Naberius.Fire2", "weapons/tfa_cso/naberius/fire2.wav", false, "^" )
TFA.AddFireSound( "Naberius.Fire3", "weapons/tfa_cso/naberius/fire3.wav", false, "^" )
TFA.AddWeaponSound( "Naberius.Fire2_Exp", "weapons/tfa_cso/naberius/fire2_exp.wav")
TFA.AddWeaponSound( "Naberius.Draw", "weapons/tfa_cso/naberius/draw.wav")
TFA.AddWeaponSound( "Naberius.Idle", "weapons/tfa_cso/naberius/idle.wav")
TFA.AddWeaponSound( "Naberius.Reload", "weapons/tfa_cso/naberius/reload.wav")
TFA.AddWeaponSound( "Naberius.Bmod", "weapons/tfa_cso/naberius/bmod.wav")