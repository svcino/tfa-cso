SWEP.Base = "tfa_melee_base"
SWEP.Category = "TFA CS:O Melees"
SWEP.PrintName = "TURBULENT-9"
SWEP.Author	= "★Bullet★" --Author Tooltip
SWEP.ViewModel = "models/weapons/tfa_cso/c_turbulent9.mdl"
SWEP.WorldModel = "models/weapons/tfa_cso/w_turbulent9_r.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 80
SWEP.UseHands = true
SWEP.HoldType = "fist"
SWEP.Type	= "Rare Grade Melee"
SWEP.DrawCrosshair = true

SWEP.Primary.Directional = false

SWEP.Spawnable = true
SWEP.AdminOnly = false
SWEP.Secondary.Automatic = true
SWEP.DisableIdleAnimations = false

SWEP.Secondary.CanBash = false
SWEP.Secondary.MaxCombo = -1
SWEP.Primary.MaxCombo = -1

SWEP.VMPos = Vector(0,0,0) --The viewmodel positional offset, constantly.  Subtract this from any other modifications to viewmodel position.

--[[INSPECTION]]--

SWEP.InspectPos = Vector (0,0,0) --Replace with a vector, in style of ironsights position, to be used for inspection
SWEP.InspectAng = Vector (0,0,0) --Replace with a vector, in style of ironsights angle, to be used for inspection
SWEP.InspectionLoop = true --Setting false will cancel inspection once the animation is done.  CS:GO style.

-- nZombies Stuff
SWEP.NZWonderWeapon		= false	-- Is this a Wonder-Weapon? If true, only one player can have it at a time. Cheats aren't stopped, though.
--SWEP.NZRePaPText		= "your text here"	-- When RePaPing, what should be shown? Example: Press E to your text here for 2000 points.
SWEP.NZPaPName				= "TURBO-BLAZED-18"
--SWEP.NZPaPReplacement 	= "tfa_cso_dualinfinityfinal"	-- If Pack-a-Punched, replace this gun with the entity class shown here.
SWEP.NZPreventBox		= false	-- If true, this gun won't be placed in random boxes GENERATED. Users can still place it in manually.
SWEP.NZTotalBlackList	= false	-- if true, this gun can't be placed in the box, even manually, and can't be bought off a wall, even if placed manually. Only code can give this gun.

SWEP.WElements = {
	["turbulent9_l"] = { type = "Model", model = "models/weapons/tfa_cso/w_turbulent9_l.mdl", bone = "ValveBiped.Bip01_L_Hand", rel = "", pos = Vector(2.5, 1, 0), angle = Angle(180, 180, 120), size = Vector(1.2, 1.2, 1.2), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.Offset = {
		Pos = {
		Up = -1,
		Right = 0,
		Forward = 2.5,
		},
		Ang = {
		Up = -190,
		Right = 190,
		Forward = 75
		},
		Scale = 1.2
}


sound.Add({
	['name'] = "TURBULENT9.Draw",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/draw.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Idle",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/idle.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash1",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash1.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash2",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash2.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.HitFlesh",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/hit1.wav", "weapons/tfa_cso/knife/hit2.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Stab",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/stab.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.HitWall",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/hitwall1.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash3_Ready",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash3_ready.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash3_Idle",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash3_idle.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "TURBULENT9.Slash3_Hit_Idle",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/turbulent9/slash3_hit_idle.wav" },
	['pitch'] = {100,100}
})

SWEP.Primary.Attacks = {
	{
		['act'] = ACT_VM_HITLEFT, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 13*5, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(60,0,0), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 45, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_SLASH, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.18, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "TURBULENT9.Slash1", -- Sound ID
		['snd_delay'] = 0.1,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 0.65, --time before next attack
		['hull'] = 48, --Hullsize
		['direction'] = "W", --Swing dir,
		['hitflesh'] = "TURBULENT9.HitFlesh",
		['hitworld'] = "TURBULENT9.HitWall"
	},
	{
		['act'] = ACT_VM_HITRIGHT, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 13*5, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(50,0,-70), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 45, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_SLASH, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.15, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "TURBULENT9.Slash2", -- Sound ID
		['snd_delay'] = 0.1,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 0.65, --time before next attack
		['hull'] = 48, --Hullsize
		['direction'] = "W", --Swing dir,
		['hitflesh'] = "TURBULENT9.HitFlesh",
		['hitworld'] = "TURBULENT9.HitWall"
	}
}