SWEP.Base = "tfa_melee_base"
SWEP.Category = "TFA CS:O Melees"
SWEP.PrintName = "THANATOS-9"

SWEP.ViewModel = "models/weapons/tfa_cso/c_thanatos9.mdl"
SWEP.WorldModel = "models/weapons/tfa_cso/w_thanatos_9.mdl"
SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 80
SWEP.UseHands = true
SWEP.HoldType = "melee2"
SWEP.DrawCrosshair = true

SWEP.Primary.Directional = false

SWEP.Spawnable = true
SWEP.AdminOnly = false

SWEP.DisableIdleAnimations = false
SWEP.ProceduralHolsterTime = 0
SWEP.Secondary.CanBash = false
SWEP.Secondary.MaxCombo = -1
SWEP.Primary.MaxCombo = -1

SWEP.VMPos = Vector(0,0,0) --The viewmodel positional offset, constantly.  Subtract this from any other modifications to viewmodel position.

-- nZombies Stuff
SWEP.NZWonderWeapon		= false	-- Is this a Wonder-Weapon? If true, only one player can have it at a time. Cheats aren't stopped, though.
--SWEP.NZRePaPText		= "your text here"	-- When RePaPing, what should be shown? Example: Press E to your text here for 2000 points.
SWEP.NZPaPName				= "Grim Reaper"
--SWEP.NZPaPReplacement 	= "tfa_cso_dualinfinityfinal"	-- If Pack-a-Punched, replace this gun with the entity class shown here.
SWEP.NZPreventBox		= false	-- If true, this gun won't be placed in random boxes GENERATED. Users can still place it in manually.
SWEP.NZTotalBlackList	= false	-- if true, this gun can't be placed in the box, even manually, and can't be bought off a wall, even if placed manually. Only code can give this gun.
SWEP.Precision = 15


SWEP.Offset = { --Procedural world model animation, defaulted for CS:S purposes.
		Pos = {
		Up = -28,
		Right = 1,
		Forward = 5,
		},
		Ang = {
		Up = -90,
		Right = 0,
		Forward = 180
		},
		Scale = 1.2
}


sound.Add({
	['name'] = "THANATOS9.Draw",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/draw.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.Swing1",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/swing_1.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.Swing2",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/swing_2.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.ChangeA1",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/changea1.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.ChangeA2",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/changea2.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.ChangeA3",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/changea3.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.ChangeA4",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/changea4.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.ChangeB1",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/changeb1.wav" },
	['pitch'] = {100,100}
})

sound.Add({
	['name'] = "THANATOS9.ChangeB2",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/changeb2.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "THANATOS9.Stab_Loop",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/stab_loop.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "THANATOS9.Stab_End",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/stab_end.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "THANATOS9.HitFlesh1",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/hit_flesh1.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "THANATOS9.HitFlesh2",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/hit_flesh2.wav" },
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "THANATOS9.HitWall",
	['channel'] = CHAN_STATIC,
	['sound'] = { "weapons/tfa_cso/thanatos9/hit_wall.wav" },
	['pitch'] = {100,100}
})

SWEP.Primary.Attacks = {
	{
		['act'] = ACT_VM_PRIMARYATTACK, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 130, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(-150,0,0), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 400, --This isn't overpowered enough, I swear!! (Meiryi : Yeah sure 400 damage is not overpowered)
		['dmgtype'] = DMG_SLASH, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 1, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "TFABaseMelee.Null", -- Sound ID
		['snd_delay'] = 1,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 1.5, --time before next attack
		['hull'] = 512, --Hullsize
		['direction'] = "W", --Swing dir,
		['hitflesh'] = "THANATOS9.HitFlesh1",
		['hitworld'] = "THANATOS9.HitWall",
		['maxhits'] = 25
	},
	{
		['act'] = ACT_VM_SECONDARYATTACK, -- Animation; ACT_VM_THINGY, ideally something unique per-sequence
		['len'] = 130, -- Trace source; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dir'] = Vector(-140,-5,0), -- Trace dir/length; X ( +right, -left ), Y ( +forward, -back ), Z ( +up, -down )
		['dmg'] = 400, --This isn't overpowered enough, I swear!!
		['dmgtype'] = DMG_SLASH, --DMG_SLASH,DMG_CRUSH, etc.
		['delay'] = 0.8, --Delay
		['spr'] = true, --Allow attack while sprinting?
		['snd'] = "TFABaseMelee.Null", -- Sound ID
		['snd_delay'] = 1,
		["viewpunch"] = Angle(0,0,0), --viewpunch angle
		['end'] = 1.5, --time before next attack
		['hull'] = 512, --Hullsize
		['direction'] = "S", --Swing dir,
		['hitflesh'] = "THANATOS9.HitFlesh2",
		['hitworld'] = "THANATOS9.HitWall",
		['maxhits'] = 1
	},
}

DEFINE_BASECLASS(SWEP.Base)

SWEP.Primary.Acts = {7,8}

SWEP.Secondary.Damage = 35
SWEP.Secondary.TraceLength = 130

SWEP.Secondary.PokeInterval = 0.05
SWEP.Secondary.MaxPokeTime = 7.5
SWEP.Secondary.SoundLoopInterval = 0.33
SWEP.Secondary.VMLoopInterval = 0.12

SWEP.Secondary.Poking = false
SWEP.Secondary.PokeStartDelay = -1
SWEP.Secondary.PokeEndDelay = -1
SWEP.Secondary.CurVMLoopTime = -1
SWEP.Secondary.CurPokeTime = -1
SWEP.Secondary.CurSoundLoopTime = -1
SWEP.Secondary.TargetPokeTime = -1
SWEP.Secondary.StartedPoke = false

SWEP.Secondary.LoopSD = Sound("weapons/tfa_cso/thanatos9/stab_loop.wav")

function SWEP:Holster(...)
	if(self.Secondary.Poking || CSO:IsTogglingSilencer(self) || CSO:CompareNextStatusTime(self)) then return end
	return BaseClass.Holster(self, ...)
end

function SWEP:Think(...)
    if(self.Secondary.StartedPoke) then
        if(self.Secondary.TargetPokeTime > CurTime()) then
            if(!self.Secondary.Poking) then
                self.Secondary.PokeStartDelay = CurTime() + 0.4
                CSO:ForceVMSequence(self, 2, -1, true)
            end
            if(self.Secondary.PokeStartDelay < CurTime()) then
                if(self.Secondary.CurSoundLoopTime < CurTime()) then
                    self:EmitSound(self.Secondary.LoopSD)
                    self.Secondary.CurSoundLoopTime = CurTime() + 0.33
                end
                if(self.Secondary.CurVMLoopTime < CurTime()) then
                    CSO:ForceVMSequence(self, 1, -1, true)
                    self.Secondary.CurVMLoopTime = CurTime() + self.Secondary.VMLoopInterval
                end
                if(self.Secondary.CurPokeTime < CurTime()) then
                    CSO:QuickMeleeTrace(self, 3, Vector(10, 10, 5), 80, self.Secondary.Damage, self.Secondary.TraceLength, "", "")
                    self.Secondary.CurPokeTime = CurTime() + self.Secondary.PokeInterval
                    self:GetOwner():SetAnimation(PLAYER_ATTACK1)
                end
            end
			self.Secondary.Poking = true
		else
			if(self.Secondary.Poking) then
				self:StopSound(self.Secondary.LoopSD)
				CSO:ForceVMSequence(self, 3, -1, true)
				CSO:SetNextActionTime(self, 4.35)
				self.Secondary.PokeEndDelay = CurTime() + 0.63
			end
			if(self.Secondary.PokeEndDelay < CurTime()) then
				self:ChooseSilenceAnim(!self:GetSilenced() )
				self:SetStatus(TFA.Enum.STATUS_SILENCER_TOGGLE)
				self:SetStatusEnd(CurTime() + self:GetActivityLength(tanim))
				self.Secondary.StartedPoke = false
			end
			self.Secondary.Poking = false
		end
	end
	BaseClass.Think(self, ...)
end

function SWEP:PrimaryAttack(...)
    if(CSO:BlockExtraActionTime(self)) then return end
    if(self:GetSilenced()) then
        if(!self.Secondary.StartedPoke) then
            self.Secondary.TargetPokeTime = CurTime() + self.Secondary.MaxPokeTime
            self.Secondary.StartedPoke = true
        end
    else
        CSO:PrimaryAttack_Melee(self, 5, 360, Vector(13, 13, 3), nil, -3, 180, self.Primary.Acts)
        self:SetNextPrimaryFire(CurTime() + self.Primary.Delay)
        self:SetStatusEnd(CurTime() + self.Primary.Delay)
    end
end

SWEP.Primary.Delay = 1.5

function SWEP:SecondaryAttack()
	if(CSO:BlockExtraActionTime(self)) then return end
	if(self:GetSilenced()) then
		if(!self.Secondary.Poking) then
			self:ChooseSilenceAnim(!self:GetSilenced() )
			self:SetStatus(TFA.Enum.STATUS_SILENCER_TOGGLE)
			self:SetStatusEnd(CurTime() + self:GetActivityLength(tanim))
		end
	else
		self:ChooseSilenceAnim(!self:GetSilenced() )
		self:SetStatus(TFA.Enum.STATUS_SILENCER_TOGGLE)
		self:SetStatusEnd(CurTime() + self:GetActivityLength(tanim))
		self.Secondary.StartedPoke = false
	end
end

if CLIENT then
	SWEP.WepSelectIconCSO = Material("vgui/killicons/tfa_cso_thanatos9")
	SWEP.DrawWeaponSelection = TFA_CSO_DrawWeaponSelection
end

function SWEP:SetupDataTables(...)
    local retVal = BaseClass.SetupDataTables(self, ...)

    self:NetworkVarTFA("Int", "LastPrimaryAttackChoice") -- self:GetLastPrimaryAttackChoice() and self:SetLastPrimaryAttackChoice(number)

    return retVal
end

function SWEP:ChoosePrimaryAttack()
    local attacks = self:GetStatL("Primary.Attacks") -- getting the SWEP.Primary.Attacks table

    local lastattack = self:GetLastPrimaryAttackChoice() -- default value is 0 so it'll start with 1 from next line

    local nextattack = lastattack + 1 -- choosing the next attack
    --if nextattack > 4 or self:GetComboCount() <= 0 then -- use this if you want choice to start from 1 when leaving mouse key (combo reset), otherwise do self:SetLastPrimaryAttackChoice(0) either in SWEP:Deploy() or SWEP:Holster()
    --   nextattack = 1
	--end
    if nextattack > 2 then -- reset the count if we're going beyond attacks count
        nextattack = 1
    end

    self:SetLastPrimaryAttackChoice(nextattack) -- remembering the current choice for next time
    return nextattack, attacks[nextattack] -- returning the key of SWEP.Primary.Attacks table and the chosen attack table itself
end